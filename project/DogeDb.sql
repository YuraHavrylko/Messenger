USE [master]
GO
/****** Object:  Database [DogeDB]    Script Date: 03.07.2016 21:39:20 ******/
CREATE DATABASE [DogeDB]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'DogeDB', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\DogeDB.mdf' , SIZE = 4288KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'DogeDB_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\DogeDB_log.ldf' , SIZE = 1088KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [DogeDB] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [DogeDB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [DogeDB] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [DogeDB] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [DogeDB] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [DogeDB] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [DogeDB] SET ARITHABORT OFF 
GO
ALTER DATABASE [DogeDB] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [DogeDB] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [DogeDB] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [DogeDB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [DogeDB] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [DogeDB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [DogeDB] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [DogeDB] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [DogeDB] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [DogeDB] SET  ENABLE_BROKER 
GO
ALTER DATABASE [DogeDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [DogeDB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [DogeDB] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [DogeDB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [DogeDB] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [DogeDB] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [DogeDB] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [DogeDB] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [DogeDB] SET  MULTI_USER 
GO
ALTER DATABASE [DogeDB] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [DogeDB] SET DB_CHAINING OFF 
GO
ALTER DATABASE [DogeDB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [DogeDB] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [DogeDB] SET DELAYED_DURABILITY = DISABLED 
GO
USE [DogeDB]
GO
/****** Object:  User [IIS APPPOOL\DefaultAppPool]    Script Date: 03.07.2016 21:39:20 ******/
CREATE USER [IIS APPPOOL\DefaultAppPool] FOR LOGIN [IIS APPPOOL\DefaultAppPool] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [IIS APPPOOL\.NET v4.5]    Script Date: 03.07.2016 21:39:20 ******/
CREATE USER [IIS APPPOOL\.NET v4.5] FOR LOGIN [IIS APPPOOL\.NET v4.5] WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_owner] ADD MEMBER [IIS APPPOOL\DefaultAppPool]
GO
ALTER ROLE [db_datareader] ADD MEMBER [IIS APPPOOL\DefaultAppPool]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [IIS APPPOOL\DefaultAppPool]
GO
ALTER ROLE [db_owner] ADD MEMBER [IIS APPPOOL\.NET v4.5]
GO
ALTER ROLE [db_datareader] ADD MEMBER [IIS APPPOOL\.NET v4.5]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [IIS APPPOOL\.NET v4.5]
GO
/****** Object:  Table [dbo].[Message]    Script Date: 03.07.2016 21:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Message](
	[MessageId] [int] IDENTITY(1,1) NOT NULL,
	[FromUser] [int] NOT NULL,
	[ToUser] [int] NOT NULL,
	[OriginalMessage] [varchar](max) NULL,
	[ModifiedMessage] [varchar](max) NULL,
	[TimeOfSending] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[MessageId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Phrases]    Script Date: 03.07.2016 21:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Phrases](
	[IdPhrase] [int] IDENTITY(1,1) NOT NULL,
	[Phrase] [varchar](max) NOT NULL,
 CONSTRAINT [Pk_phrases] PRIMARY KEY CLUSTERED 
(
	[IdPhrase] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[User]    Script Date: 03.07.2016 21:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[UserId] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](50) NOT NULL,
	[Password] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK__User__1788CC4CD333A24F] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[WhatIs]    Script Date: 03.07.2016 21:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WhatIs](
	[ArticleId] [int] IDENTITY(1,1) NOT NULL,
	[ArticleName] [nvarchar](50) NOT NULL,
	[ArticleParagraph] [nvarchar](2000) NOT NULL,
 CONSTRAINT [PK_Article] PRIMARY KEY CLUSTERED 
(
	[ArticleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[Message] ON 

INSERT [dbo].[Message] ([MessageId], [FromUser], [ToUser], [OriginalMessage], [ModifiedMessage], [TimeOfSending]) VALUES (1, 1, 2, N'hello', N'hello', CAST(N'2016-05-25 12:10:00.000' AS DateTime))
INSERT [dbo].[Message] ([MessageId], [FromUser], [ToUser], [OriginalMessage], [ModifiedMessage], [TimeOfSending]) VALUES (2, 1, 4, N'hello', N'hello', CAST(N'2016-05-25 13:10:00.000' AS DateTime))
INSERT [dbo].[Message] ([MessageId], [FromUser], [ToUser], [OriginalMessage], [ModifiedMessage], [TimeOfSending]) VALUES (3, 3, 1, N'hello', N'hello', CAST(N'2016-05-25 14:10:00.000' AS DateTime))
INSERT [dbo].[Message] ([MessageId], [FromUser], [ToUser], [OriginalMessage], [ModifiedMessage], [TimeOfSending]) VALUES (4, 2, 1, N'hello', N'hello', CAST(N'2016-05-25 15:10:00.000' AS DateTime))
INSERT [dbo].[Message] ([MessageId], [FromUser], [ToUser], [OriginalMessage], [ModifiedMessage], [TimeOfSending]) VALUES (5, 5, 1, N'hay', N'hay', CAST(N'2016-05-25 16:10:00.000' AS DateTime))
INSERT [dbo].[Message] ([MessageId], [FromUser], [ToUser], [OriginalMessage], [ModifiedMessage], [TimeOfSending]) VALUES (6, 3, 1, N'how you doing', N'how you doing', CAST(N'2016-05-25 17:10:00.000' AS DateTime))
INSERT [dbo].[Message] ([MessageId], [FromUser], [ToUser], [OriginalMessage], [ModifiedMessage], [TimeOfSending]) VALUES (7, 3, 2, N'what''s up', N'what''s up', CAST(N'2016-05-25 12:18:00.000' AS DateTime))
INSERT [dbo].[Message] ([MessageId], [FromUser], [ToUser], [OriginalMessage], [ModifiedMessage], [TimeOfSending]) VALUES (8, 3, 2, N'hay', N'hay', CAST(N'2016-05-25 12:19:00.000' AS DateTime))
INSERT [dbo].[Message] ([MessageId], [FromUser], [ToUser], [OriginalMessage], [ModifiedMessage], [TimeOfSending]) VALUES (9, 6, 1, N'Hi, vova. 2+2*2', N'Hi, vova.6', CAST(N'2016-05-25 20:18:00.000' AS DateTime))
INSERT [dbo].[Message] ([MessageId], [FromUser], [ToUser], [OriginalMessage], [ModifiedMessage], [TimeOfSending]) VALUES (10, 7, 1, N'Hi, Vova! http://stackoverflow.com/', N'Hi, Vova! <a href="http://stackoverflow.com/">Stack Overflow</a>', CAST(N'2016-05-25 21:21:00.000' AS DateTime))
INSERT [dbo].[Message] ([MessageId], [FromUser], [ToUser], [OriginalMessage], [ModifiedMessage], [TimeOfSending]) VALUES (11, 7, 6, N'Hi)', N'Hi)', CAST(N'2016-05-25 22:22:00.000' AS DateTime))
INSERT [dbo].[Message] ([MessageId], [FromUser], [ToUser], [OriginalMessage], [ModifiedMessage], [TimeOfSending]) VALUES (12, 7, 1, N'Hi)', N'Hi)', CAST(N'2016-05-25 23:18:00.000' AS DateTime))
INSERT [dbo].[Message] ([MessageId], [FromUser], [ToUser], [OriginalMessage], [ModifiedMessage], [TimeOfSending]) VALUES (13, 7, 1, N'yo', N'yo', CAST(N'2016-05-26 12:18:00.000' AS DateTime))
INSERT [dbo].[Message] ([MessageId], [FromUser], [ToUser], [OriginalMessage], [ModifiedMessage], [TimeOfSending]) VALUES (14, 6, 1, N'hi)))', N'hi)))', CAST(N'2016-05-27 12:18:00.000' AS DateTime))
INSERT [dbo].[Message] ([MessageId], [FromUser], [ToUser], [OriginalMessage], [ModifiedMessage], [TimeOfSending]) VALUES (15, 6, 7, N'Where is mark 43?', N'Where is mark 43?', CAST(N'2016-05-28 12:18:00.000' AS DateTime))
INSERT [dbo].[Message] ([MessageId], [FromUser], [ToUser], [OriginalMessage], [ModifiedMessage], [TimeOfSending]) VALUES (18, 16, 1, N'a', N'a', CAST(N'2016-05-29 12:18:00.000' AS DateTime))
INSERT [dbo].[Message] ([MessageId], [FromUser], [ToUser], [OriginalMessage], [ModifiedMessage], [TimeOfSending]) VALUES (19, 6, 1, N'Jak sia mash?', N'Jak sia mash?', CAST(N'2016-05-30 12:18:00.000' AS DateTime))
INSERT [dbo].[Message] ([MessageId], [FromUser], [ToUser], [OriginalMessage], [ModifiedMessage], [TimeOfSending]) VALUES (21, 1, 6, N'hi', N'hi', CAST(N'2016-06-10 12:18:00.000' AS DateTime))
INSERT [dbo].[Message] ([MessageId], [FromUser], [ToUser], [OriginalMessage], [ModifiedMessage], [TimeOfSending]) VALUES (22, 6, 3, N'hi, petya', N'hi, petya', CAST(N'2016-06-11 12:18:00.000' AS DateTime))
INSERT [dbo].[Message] ([MessageId], [FromUser], [ToUser], [OriginalMessage], [ModifiedMessage], [TimeOfSending]) VALUES (23, 6, 5, N'hi, volo', N'hi, volo', CAST(N'2016-06-12 12:18:00.000' AS DateTime))
INSERT [dbo].[Message] ([MessageId], [FromUser], [ToUser], [OriginalMessage], [ModifiedMessage], [TimeOfSending]) VALUES (24, 6, 4, N'hi, ihor', N'hi, ihor', CAST(N'2016-06-13 12:18:00.000' AS DateTime))
INSERT [dbo].[Message] ([MessageId], [FromUser], [ToUser], [OriginalMessage], [ModifiedMessage], [TimeOfSending]) VALUES (44, 6, 1, N'all dialog test', N'all dialog test', CAST(N'2016-06-25 20:23:29.877' AS DateTime))
INSERT [dbo].[Message] ([MessageId], [FromUser], [ToUser], [OriginalMessage], [ModifiedMessage], [TimeOfSending]) VALUES (45, 31, 32, N'u21', N'u21', CAST(N'2016-06-27 19:51:08.487' AS DateTime))
INSERT [dbo].[Message] ([MessageId], [FromUser], [ToUser], [OriginalMessage], [ModifiedMessage], [TimeOfSending]) VALUES (46, 31, 33, N'u22', N'u22', CAST(N'2016-06-27 19:51:22.170' AS DateTime))
INSERT [dbo].[Message] ([MessageId], [FromUser], [ToUser], [OriginalMessage], [ModifiedMessage], [TimeOfSending]) VALUES (47, 31, 34, N'u23', N'u23', CAST(N'2016-06-27 19:51:28.657' AS DateTime))
INSERT [dbo].[Message] ([MessageId], [FromUser], [ToUser], [OriginalMessage], [ModifiedMessage], [TimeOfSending]) VALUES (48, 31, 35, N'u24', N'u24', CAST(N'2016-06-27 19:51:35.590' AS DateTime))
INSERT [dbo].[Message] ([MessageId], [FromUser], [ToUser], [OriginalMessage], [ModifiedMessage], [TimeOfSending]) VALUES (49, 32, 36, N'u31', N'u31', CAST(N'2016-06-27 19:52:06.173' AS DateTime))
INSERT [dbo].[Message] ([MessageId], [FromUser], [ToUser], [OriginalMessage], [ModifiedMessage], [TimeOfSending]) VALUES (50, 36, 40, N'u41', N'u41', CAST(N'2016-06-27 19:52:28.750' AS DateTime))
INSERT [dbo].[Message] ([MessageId], [FromUser], [ToUser], [OriginalMessage], [ModifiedMessage], [TimeOfSending]) VALUES (51, 44, 40, N'u41', N'u41', CAST(N'2016-06-27 19:52:56.133' AS DateTime))
INSERT [dbo].[Message] ([MessageId], [FromUser], [ToUser], [OriginalMessage], [ModifiedMessage], [TimeOfSending]) VALUES (52, 44, 48, N'u61', N'u61', CAST(N'2016-06-27 19:53:04.563' AS DateTime))
INSERT [dbo].[Message] ([MessageId], [FromUser], [ToUser], [OriginalMessage], [ModifiedMessage], [TimeOfSending]) VALUES (53, 33, 37, N'u32', N'u32', CAST(N'2016-06-27 19:53:43.523' AS DateTime))
INSERT [dbo].[Message] ([MessageId], [FromUser], [ToUser], [OriginalMessage], [ModifiedMessage], [TimeOfSending]) VALUES (54, 37, 41, N'u42', N'u42', CAST(N'2016-06-27 19:54:17.387' AS DateTime))
INSERT [dbo].[Message] ([MessageId], [FromUser], [ToUser], [OriginalMessage], [ModifiedMessage], [TimeOfSending]) VALUES (56, 41, 45, N'u52', N'u52', CAST(N'2016-06-27 19:54:59.397' AS DateTime))
INSERT [dbo].[Message] ([MessageId], [FromUser], [ToUser], [OriginalMessage], [ModifiedMessage], [TimeOfSending]) VALUES (57, 45, 48, N'u61', N'u61', CAST(N'2016-06-27 19:55:20.120' AS DateTime))
INSERT [dbo].[Message] ([MessageId], [FromUser], [ToUser], [OriginalMessage], [ModifiedMessage], [TimeOfSending]) VALUES (58, 34, 38, N'u33', N'u33', CAST(N'2016-06-27 19:55:41.530' AS DateTime))
INSERT [dbo].[Message] ([MessageId], [FromUser], [ToUser], [OriginalMessage], [ModifiedMessage], [TimeOfSending]) VALUES (59, 33, 42, N'u43', N'u43', CAST(N'2016-06-27 19:57:47.250' AS DateTime))
INSERT [dbo].[Message] ([MessageId], [FromUser], [ToUser], [OriginalMessage], [ModifiedMessage], [TimeOfSending]) VALUES (60, 38, 42, N'u43', N'u43', CAST(N'2016-06-27 20:00:27.627' AS DateTime))
INSERT [dbo].[Message] ([MessageId], [FromUser], [ToUser], [OriginalMessage], [ModifiedMessage], [TimeOfSending]) VALUES (61, 42, 46, N'u53', N'u53', CAST(N'2016-06-27 20:00:57.467' AS DateTime))
INSERT [dbo].[Message] ([MessageId], [FromUser], [ToUser], [OriginalMessage], [ModifiedMessage], [TimeOfSending]) VALUES (62, 46, 48, N'u61', N'u61', CAST(N'2016-06-27 20:01:15.350' AS DateTime))
INSERT [dbo].[Message] ([MessageId], [FromUser], [ToUser], [OriginalMessage], [ModifiedMessage], [TimeOfSending]) VALUES (63, 35, 39, N'u34', N'u34', CAST(N'2016-06-27 20:01:40.027' AS DateTime))
INSERT [dbo].[Message] ([MessageId], [FromUser], [ToUser], [OriginalMessage], [ModifiedMessage], [TimeOfSending]) VALUES (64, 39, 43, N'u44', N'u44', CAST(N'2016-06-27 20:01:54.333' AS DateTime))
INSERT [dbo].[Message] ([MessageId], [FromUser], [ToUser], [OriginalMessage], [ModifiedMessage], [TimeOfSending]) VALUES (65, 43, 47, N'u54', N'u54', CAST(N'2016-06-27 20:02:48.900' AS DateTime))
INSERT [dbo].[Message] ([MessageId], [FromUser], [ToUser], [OriginalMessage], [ModifiedMessage], [TimeOfSending]) VALUES (66, 47, 48, N'u61', N'u61', CAST(N'2016-06-27 20:03:12.210' AS DateTime))
INSERT [dbo].[Message] ([MessageId], [FromUser], [ToUser], [OriginalMessage], [ModifiedMessage], [TimeOfSending]) VALUES (67, 6, 1, N'timeout test', N'timeout test', CAST(N'2016-06-30 08:26:26.607' AS DateTime))
INSERT [dbo].[Message] ([MessageId], [FromUser], [ToUser], [OriginalMessage], [ModifiedMessage], [TimeOfSending]) VALUES (68, 6, 3, N'Happy Birthday!', N'Happy Birthday!', CAST(N'2016-07-01 08:43:35.567' AS DateTime))
INSERT [dbo].[Message] ([MessageId], [FromUser], [ToUser], [OriginalMessage], [ModifiedMessage], [TimeOfSending]) VALUES (69, 6, 3, N'hi', N'hi', CAST(N'2016-07-01 22:09:54.900' AS DateTime))
INSERT [dbo].[Message] ([MessageId], [FromUser], [ToUser], [OriginalMessage], [ModifiedMessage], [TimeOfSending]) VALUES (70, 1, 6, N'test passed?', N'test passed?', CAST(N'2016-07-01 22:12:14.820' AS DateTime))
INSERT [dbo].[Message] ([MessageId], [FromUser], [ToUser], [OriginalMessage], [ModifiedMessage], [TimeOfSending]) VALUES (71, 6, 1, N'yep)', N'yep)', CAST(N'2016-07-01 22:12:47.153' AS DateTime))
INSERT [dbo].[Message] ([MessageId], [FromUser], [ToUser], [OriginalMessage], [ModifiedMessage], [TimeOfSending]) VALUES (72, 1, 6, N'cool)', N'cool)', CAST(N'2016-07-01 22:14:04.547' AS DateTime))
INSERT [dbo].[Message] ([MessageId], [FromUser], [ToUser], [OriginalMessage], [ModifiedMessage], [TimeOfSending]) VALUES (73, 1, 31, N'Hi. u11)', N'Hi. u11)', CAST(N'2016-07-01 22:16:26.483' AS DateTime))
INSERT [dbo].[Message] ([MessageId], [FromUser], [ToUser], [OriginalMessage], [ModifiedMessage], [TimeOfSending]) VALUES (74, 4, 5, N'Hi, volo)', N'Hi, volo)', CAST(N'2016-07-01 22:22:53.360' AS DateTime))
INSERT [dbo].[Message] ([MessageId], [FromUser], [ToUser], [OriginalMessage], [ModifiedMessage], [TimeOfSending]) VALUES (75, 4, 6, N'hi, liubomyr', N'hi, liubomyr', CAST(N'2016-07-01 22:23:12.673' AS DateTime))
INSERT [dbo].[Message] ([MessageId], [FromUser], [ToUser], [OriginalMessage], [ModifiedMessage], [TimeOfSending]) VALUES (76, 6, 1, N'refactor', N'refactor', CAST(N'2016-07-02 01:59:40.283' AS DateTime))
INSERT [dbo].[Message] ([MessageId], [FromUser], [ToUser], [OriginalMessage], [ModifiedMessage], [TimeOfSending]) VALUES (77, 6, 5, N'Where are you from?', N'Where are you from?', CAST(N'2016-07-02 02:28:59.657' AS DateTime))
INSERT [dbo].[Message] ([MessageId], [FromUser], [ToUser], [OriginalMessage], [ModifiedMessage], [TimeOfSending]) VALUES (78, 6, 5, N'Sloltvyn?', N'Sloltvyn?', CAST(N'2016-07-02 12:10:16.543' AS DateTime))
INSERT [dbo].[Message] ([MessageId], [FromUser], [ToUser], [OriginalMessage], [ModifiedMessage], [TimeOfSending]) VALUES (79, 6, 1, N'Dialog refresh test', N'Dialog refresh test', CAST(N'2016-07-02 12:13:49.750' AS DateTime))
INSERT [dbo].[Message] ([MessageId], [FromUser], [ToUser], [OriginalMessage], [ModifiedMessage], [TimeOfSending]) VALUES (80, 6, 4, N'Refresh test', N'Refresh test', CAST(N'2016-07-02 12:18:08.633' AS DateTime))
INSERT [dbo].[Message] ([MessageId], [FromUser], [ToUser], [OriginalMessage], [ModifiedMessage], [TimeOfSending]) VALUES (81, 6, 5, N'That Solotvyn with lakes?', N'That Solotvyn with lakes?', CAST(N'2016-07-02 12:19:13.253' AS DateTime))
INSERT [dbo].[Message] ([MessageId], [FromUser], [ToUser], [OriginalMessage], [ModifiedMessage], [TimeOfSending]) VALUES (82, 6, 4, N'Scroll now works)', N'Scroll now works)', CAST(N'2016-07-02 12:20:31.117' AS DateTime))
INSERT [dbo].[Message] ([MessageId], [FromUser], [ToUser], [OriginalMessage], [ModifiedMessage], [TimeOfSending]) VALUES (83, 6, 3, N'Cleraing TextBox', N'Cleraing TextBox', CAST(N'2016-07-02 12:22:20.500' AS DateTime))
INSERT [dbo].[Message] ([MessageId], [FromUser], [ToUser], [OriginalMessage], [ModifiedMessage], [TimeOfSending]) VALUES (84, 6, 4, N'three days left)', N'three days left)', CAST(N'2016-07-02 18:49:11.923' AS DateTime))
INSERT [dbo].[Message] ([MessageId], [FromUser], [ToUser], [OriginalMessage], [ModifiedMessage], [TimeOfSending]) VALUES (85, 6, 8, N'hi', N'hi', CAST(N'2016-07-02 21:52:36.137' AS DateTime))
INSERT [dbo].[Message] ([MessageId], [FromUser], [ToUser], [OriginalMessage], [ModifiedMessage], [TimeOfSending]) VALUES (86, 6, 1, N'test', N'test', CAST(N'2016-07-02 21:56:39.347' AS DateTime))
INSERT [dbo].[Message] ([MessageId], [FromUser], [ToUser], [OriginalMessage], [ModifiedMessage], [TimeOfSending]) VALUES (87, 1, 31, N'How are you?', N'How are you?', CAST(N'2016-07-03 00:18:48.757' AS DateTime))
INSERT [dbo].[Message] ([MessageId], [FromUser], [ToUser], [OriginalMessage], [ModifiedMessage], [TimeOfSending]) VALUES (88, 1, 6, N'passed?', N'passed?', CAST(N'2016-07-03 00:19:14.573' AS DateTime))
INSERT [dbo].[Message] ([MessageId], [FromUser], [ToUser], [OriginalMessage], [ModifiedMessage], [TimeOfSending]) VALUES (89, 6, 1, N'Modal window', N'Modal window', CAST(N'2016-07-03 02:08:22.290' AS DateTime))
INSERT [dbo].[Message] ([MessageId], [FromUser], [ToUser], [OriginalMessage], [ModifiedMessage], [TimeOfSending]) VALUES (90, 6, 8, N'Angular test', N'Angular test', CAST(N'2016-07-03 12:16:57.727' AS DateTime))
INSERT [dbo].[Message] ([MessageId], [FromUser], [ToUser], [OriginalMessage], [ModifiedMessage], [TimeOfSending]) VALUES (91, 1, 6, N'Hi, did you pass exam?', N'Hi, did you pass exam?', CAST(N'2016-07-03 12:31:08.253' AS DateTime))
INSERT [dbo].[Message] ([MessageId], [FromUser], [ToUser], [OriginalMessage], [ModifiedMessage], [TimeOfSending]) VALUES (92, 6, 1, N'yes)', N'yes)', CAST(N'2016-07-03 12:31:55.213' AS DateTime))
INSERT [dbo].[Message] ([MessageId], [FromUser], [ToUser], [OriginalMessage], [ModifiedMessage], [TimeOfSending]) VALUES (94, 1, 6, N'congratulations)', N'congratulations)', CAST(N'2016-07-03 12:40:35.797' AS DateTime))
INSERT [dbo].[Message] ([MessageId], [FromUser], [ToUser], [OriginalMessage], [ModifiedMessage], [TimeOfSending]) VALUES (95, 6, 1, N'thanks)', N'thanks)', CAST(N'2016-07-03 12:43:17.400' AS DateTime))
INSERT [dbo].[Message] ([MessageId], [FromUser], [ToUser], [OriginalMessage], [ModifiedMessage], [TimeOfSending]) VALUES (96, 6, 1, N'how about you? did you pass?', N'how about you? did you pass?', CAST(N'2016-07-03 12:43:37.160' AS DateTime))
INSERT [dbo].[Message] ([MessageId], [FromUser], [ToUser], [OriginalMessage], [ModifiedMessage], [TimeOfSending]) VALUES (97, 1, 6, N'yes)', N'yes)', CAST(N'2016-07-03 12:49:10.620' AS DateTime))
INSERT [dbo].[Message] ([MessageId], [FromUser], [ToUser], [OriginalMessage], [ModifiedMessage], [TimeOfSending]) VALUES (98, 6, 1, N'congratulations)', N'congratulations)', CAST(N'2016-07-03 12:49:26.110' AS DateTime))
INSERT [dbo].[Message] ([MessageId], [FromUser], [ToUser], [OriginalMessage], [ModifiedMessage], [TimeOfSending]) VALUES (99, 6, 1, N'What about course work?', N'What about course work?', CAST(N'2016-07-03 16:54:38.300' AS DateTime))
INSERT [dbo].[Message] ([MessageId], [FromUser], [ToUser], [OriginalMessage], [ModifiedMessage], [TimeOfSending]) VALUES (100, 1, 6, N'Got A)', N'Got A)', CAST(N'2016-07-03 16:55:09.980' AS DateTime))
INSERT [dbo].[Message] ([MessageId], [FromUser], [ToUser], [OriginalMessage], [ModifiedMessage], [TimeOfSending]) VALUES (101, 1, 6, N'How are you?', N'How are you?', CAST(N'2016-07-03 17:48:26.293' AS DateTime))
INSERT [dbo].[Message] ([MessageId], [FromUser], [ToUser], [OriginalMessage], [ModifiedMessage], [TimeOfSending]) VALUES (102, 6, 1, N'Good)', N'Good)', CAST(N'2016-07-03 17:48:42.127' AS DateTime))
INSERT [dbo].[Message] ([MessageId], [FromUser], [ToUser], [OriginalMessage], [ModifiedMessage], [TimeOfSending]) VALUES (103, 6, 1, N'What will you do on holidays?', N'What will you do on holidays?', CAST(N'2016-07-03 20:13:31.400' AS DateTime))
INSERT [dbo].[Message] ([MessageId], [FromUser], [ToUser], [OriginalMessage], [ModifiedMessage], [TimeOfSending]) VALUES (104, 6, 1, N'Will you go to London?', N'Will you go to London?', CAST(N'2016-07-03 20:14:05.143' AS DateTime))
INSERT [dbo].[Message] ([MessageId], [FromUser], [ToUser], [OriginalMessage], [ModifiedMessage], [TimeOfSending]) VALUES (105, 4, 3, N'Hi, petya)', N'Hi, petya)', CAST(N'2016-07-03 21:28:42.350' AS DateTime))
SET IDENTITY_INSERT [dbo].[Message] OFF
SET IDENTITY_INSERT [dbo].[Phrases] ON 

INSERT [dbo].[Phrases] ([IdPhrase], [Phrase]) VALUES (1, N'There used to be a street named after Chuck Norris, but it was changed because nobody crosses Chuck Norris and lives')
INSERT [dbo].[Phrases] ([IdPhrase], [Phrase]) VALUES (2, N'Chuck Norris counted to infinity - twice.')
INSERT [dbo].[Phrases] ([IdPhrase], [Phrase]) VALUES (3, N'Chuck Norris is the reason why Waldo is hiding.')
INSERT [dbo].[Phrases] ([IdPhrase], [Phrase]) VALUES (4, N'Chuck Norris can slam a revolving door')
SET IDENTITY_INSERT [dbo].[Phrases] OFF
SET IDENTITY_INSERT [dbo].[User] ON 

INSERT [dbo].[User] ([UserId], [UserName], [Password]) VALUES (1, N'vova', N'C5A4E7E6882845EA7BB4D9462868219B')
INSERT [dbo].[User] ([UserId], [UserName], [Password]) VALUES (2, N'vasya', N'6351BF9DCE654515BF1DDBD6426DFA97')
INSERT [dbo].[User] ([UserId], [UserName], [Password]) VALUES (3, N'petya', N'96055F5B06BF9381AC43879351642CF5')
INSERT [dbo].[User] ([UserId], [UserName], [Password]) VALUES (4, N'ihor', N'008BD5AD93B754D500338C253D9C1770')
INSERT [dbo].[User] ([UserId], [UserName], [Password]) VALUES (5, N'volo', N'C5A4E7E6882845EA7BB4D9462868219B')
INSERT [dbo].[User] ([UserId], [UserName], [Password]) VALUES (6, N'liubomyr', N'F4EB98EAACE390B5831A84F826E247CA')
INSERT [dbo].[User] ([UserId], [UserName], [Password]) VALUES (7, N'TonyStark', N'1632D48351ADCE7585FE9C359BE515FF')
INSERT [dbo].[User] ([UserId], [UserName], [Password]) VALUES (8, N'temp', N'3D801AA532C1CEC3EE82D87A99FDF63F')
INSERT [dbo].[User] ([UserId], [UserName], [Password]) VALUES (25, N'temp1', N'78475D71E72A3C181C077CB779475F1E')
INSERT [dbo].[User] ([UserId], [UserName], [Password]) VALUES (31, N'u11', N'A4CB5CF0C26C130189BC9536BD787515')
INSERT [dbo].[User] ([UserId], [UserName], [Password]) VALUES (32, N'u21', N'22A8B76F85A16947E96FA3C07F5D67BF')
INSERT [dbo].[User] ([UserId], [UserName], [Password]) VALUES (33, N'u22', N'4DABFC91400E263D241D73A9D71A07BE')
INSERT [dbo].[User] ([UserId], [UserName], [Password]) VALUES (34, N'u23', N'45F1EC947069C5FBECC675A77123D6E4')
INSERT [dbo].[User] ([UserId], [UserName], [Password]) VALUES (35, N'u24', N'295FE5C84ADEAE4243389575474CAE81')
INSERT [dbo].[User] ([UserId], [UserName], [Password]) VALUES (36, N'u31', N'66B293A5186136435DF58A57A3A50F43')
INSERT [dbo].[User] ([UserId], [UserName], [Password]) VALUES (37, N'u32', N'A311A831B48D59687CF0822A612C9032')
INSERT [dbo].[User] ([UserId], [UserName], [Password]) VALUES (38, N'u33', N'72DA1B188E6B4DD1FB37A5D8150C820C')
INSERT [dbo].[User] ([UserId], [UserName], [Password]) VALUES (39, N'u34', N'2C329FE9E23F1C513758D15087F4D6E8')
INSERT [dbo].[User] ([UserId], [UserName], [Password]) VALUES (40, N'u41', N'E5BCF9ECE250382026990FD9EC0752D2')
INSERT [dbo].[User] ([UserId], [UserName], [Password]) VALUES (41, N'u42', N'1C7082C84E576341DE713934775FF522')
INSERT [dbo].[User] ([UserId], [UserName], [Password]) VALUES (42, N'u43', N'1A01BB6274C1306D6CEC9BBCEF977051')
INSERT [dbo].[User] ([UserId], [UserName], [Password]) VALUES (43, N'u44', N'6D6FC48880C555664B998D38AB1CECFA')
INSERT [dbo].[User] ([UserId], [UserName], [Password]) VALUES (44, N'u51', N'A80F5884460F3875107776C58C1B40EB')
INSERT [dbo].[User] ([UserId], [UserName], [Password]) VALUES (45, N'u52', N'36E88B16BFEC2AB7B84E674ADB99FB8B')
INSERT [dbo].[User] ([UserId], [UserName], [Password]) VALUES (46, N'u53', N'890A67EB13B7BC800DCCB049CCECA5A0')
INSERT [dbo].[User] ([UserId], [UserName], [Password]) VALUES (47, N'u54', N'DEEB43375C12FAC75456EDDD35DCDB40')
INSERT [dbo].[User] ([UserId], [UserName], [Password]) VALUES (48, N'u61', N'EBE2C8DC271410A7A7B5B55CAE772A98')
INSERT [dbo].[User] ([UserId], [UserName], [Password]) VALUES (49, N'temp10', N'C748F6C8E4FB4CE0EDCC02ECDE793093')
INSERT [dbo].[User] ([UserId], [UserName], [Password]) VALUES (51, N'temp11', N'9495CB81CBEA6D50C7E38BBD0B2015FB')
INSERT [dbo].[User] ([UserId], [UserName], [Password]) VALUES (52, N'temp12', N'331C9E0A6EE6921359CED67D6945603F')
INSERT [dbo].[User] ([UserId], [UserName], [Password]) VALUES (54, N'temp228', N'769E15950113660BC23E1CF9C19DE45C')
SET IDENTITY_INSERT [dbo].[User] OFF
SET IDENTITY_INSERT [dbo].[WhatIs] ON 

INSERT [dbo].[WhatIs] ([ArticleId], [ArticleName], [ArticleParagraph]) VALUES (1, N'football', N'Football is a family of team sports that involve, to varying degrees, kicking a ball to score a goal. Unqualified, the word football is understood to refer to whichever form of football is the most popular in the regional context in which the word appears. Sports commonly called ''football'' in certain places include: association football (known as soccer in some countries); gridiron football (specifically American football or Canadian football); Australian rules football; rugby football (either rugby league or rugby union); and Gaelic football.[1][2] These different variations of football are known as football codes.')
INSERT [dbo].[WhatIs] ([ArticleId], [ArticleName], [ArticleParagraph]) VALUES (2, N'dependency injection', N'In software engineering, dependency injection is a software design pattern that implements inversion of control for resolving dependencies. A dependency is an object that can be used (a service). An injection is the passing of a dependency to a dependent object (a client) that would use it. The service is made part of the client''s state.[1] Passing the service to the client, rather than allowing a client to build or find the service, is the fundamental requirement of the pattern.')
INSERT [dbo].[WhatIs] ([ArticleId], [ArticleName], [ArticleParagraph]) VALUES (3, N'ukraine', N'Ukraine (i/juːˈkreɪn/; Ukrainian: Україна, tr. Ukrayina [ukrɑˈjinɑ]) is a sovereign state in Eastern Europe,[9][10][11][12] bordered by Russia to the east and northeast, Belarus to the northwest, Poland and Slovakia to the west, Hungary, Romania, and Moldova to the southwest, and the Black Sea and Sea of Azov to the south and southeast, respectively. Ukraine is currently in territorial dispute with Russia over the Crimean Peninsula which Russia annexed in 2014 but which Ukraine and most of the international community recognise as Ukrainian. Including Crimea, Ukraine has an area of 603,628&#160;km2 (233,062&#160;sq&#160;mi), making it the largest country entirely within Europe and the 46th largest country in the world, and a population of about 44.5 million, making it the 32nd most populous country in the world.')
INSERT [dbo].[WhatIs] ([ArticleId], [ArticleName], [ArticleParagraph]) VALUES (4, N'Chuck Norris', N'Carlos Ray "Chuck" Norris (born March 10, 1940) is an American martial artist, actor, film producer and screenwriter. After serving in the United States Air Force, he began his rise to fame as a martial artist, and has since founded his own school, Chun Kuk Do.')
INSERT [dbo].[WhatIs] ([ArticleId], [ArticleName], [ArticleParagraph]) VALUES (5, N'oop', N'OOP, Oop, or oop may refer to:')
INSERT [dbo].[WhatIs] ([ArticleId], [ArticleName], [ArticleParagraph]) VALUES (6, N'c#', N'C is the third letter in the English alphabet and a letter of the alphabets of many other writing systems which inherited it from the Latin alphabet. It is also the third letter of the ISO basic Latin alphabet. It is named cee (pronounced /ˈsiː/) in English.[1]')
INSERT [dbo].[WhatIs] ([ArticleId], [ArticleName], [ArticleParagraph]) VALUES (7, N'java', N'Java (Indonesian: Jawa; Javanese: ꦗꦮ; Sundanese: ᮏᮝ) is an island of Indonesia. With a population of over 141 million (the island itself) or 145 million (the administrative region) as of 2015[update] Census released in December 2015,[1] Java is home to 56.7 percent of the Indonesian population, and is the most populous island on Earth. The Indonesian capital city, Jakarta, is located on western Java. Much of Indonesian history took place on Java. It was the center of powerful Hindu-Buddhist empires, the Islamic sultanates, and the core of the colonial Dutch East Indies. Java was also the center of the Indonesian struggle for independence during the 1930s and 1940s. Java dominates Indonesia politically, economically and culturally.')
INSERT [dbo].[WhatIs] ([ArticleId], [ArticleName], [ArticleParagraph]) VALUES (8, N'c++', N'C++ (pronounced as cee plus plus, /ˈsiː plʌs plʌs/) is a general-purpose programming language. It has imperative, object-oriented and generic programming features, while also providing facilities for low-level memory manipulation.')
INSERT [dbo].[WhatIs] ([ArticleId], [ArticleName], [ArticleParagraph]) VALUES (9, N'game of thrones', N'Game of Thrones is an American fantasy drama television series created by David Benioff and D. B. Weiss. It is an adaptation of A Song of Ice and Fire, George R. R. Martin''s series of fantasy novels, the first of which is titled A Game of Thrones. It is filmed at Titanic Studios in Belfast and on location elsewhere in Northern Ireland, as well as in Croatia, Iceland, Malta, Morocco, Spain, Scotland, and the United States. The series premiered on HBO in the United States on April 17, 2011, and its sixth season concluded on June 26, 2016. The series was renewed for a seventh season, which is scheduled to air in 2017 with a total of seven episodes.[5][6]')
INSERT [dbo].[WhatIs] ([ArticleId], [ArticleName], [ArticleParagraph]) VALUES (10, N'rugby', N'Rugby may refer to:')
INSERT [dbo].[WhatIs] ([ArticleId], [ArticleName], [ArticleParagraph]) VALUES (11, N'baseball', N'Baseball is a bat-and-ball game played between two teams of nine players each, who take turns batting and fielding.')
SET IDENTITY_INSERT [dbo].[WhatIs] OFF
/****** Object:  StoredProcedure [dbo].[CheckUserExistence]    Script Date: 03.07.2016 21:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[CheckUserExistence]
			@currentUserName varchar(50)
AS
BEGIN
	select userId from [User] where UserName = @currentUserName 
END



GO
/****** Object:  StoredProcedure [dbo].[CheckUserExistenceById]    Script Date: 03.07.2016 21:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[CheckUserExistenceById]
			@userId int
AS
BEGIN
	select userId from [User] where userId = @userId 
END

GO
/****** Object:  StoredProcedure [dbo].[GetDialog]    Script Date: 03.07.2016 21:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[GetDialog]
	@CurrentUserId int
	,@SecondUserId int
as
begin
;with DialogId as
(
	select MessageId, FromUser, ToUser, ModifiedMessage, TimeOfSending
	from [Message]
	where (FromUser = @CurrentUserId and ToUser = @SecondUserId)
	or (FromUser = @SecondUserId and ToUser = @CurrentUserId)
)
,FromUserName as
(
	select d.MessageId, u.UserName, d.ModifiedMessage, d.TimeOfSending
	from [DialogId] d
	inner join [User] u
	on d.FromUser = u.UserId
)
,ToUserName as
(
	select d.MessageId, u.UserName
	from [DialogId] d
	inner join [User] u
	on d.ToUser = u.UserId
)

select uf.MessageId, uf.UserName [FromUser], ut.UserName [ToUser], uf.ModifiedMessage [Message], uf.TimeOfSending
from FromUserName uf
inner join ToUserName ut
on uf.MessageId = ut.MessageId

end

GO
/****** Object:  StoredProcedure [dbo].[GetDialogFromDate]    Script Date: 03.07.2016 21:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[GetDialogFromDate]
	@CurrentUserId int
	,@SecondUserId int
	,@FromDate datetime
as
begin
;with DialogId as
(
	select MessageId, FromUser, ToUser, ModifiedMessage, TimeOfSending
	from [Message]
	where ((FromUser = @CurrentUserId and ToUser = @SecondUserId)
	or (FromUser = @SecondUserId and ToUser = @CurrentUserId))
	and TimeOfSending >= @FromDate
)
,FromUserName as
(
	select d.MessageId, u.UserName, d.ModifiedMessage, d.TimeOfSending
	from [DialogId] d
	inner join [User] u
	on d.FromUser = u.UserId
)
,ToUserName as
(
	select d.MessageId, u.UserName
	from [DialogId] d
	inner join [User] u
	on d.ToUser = u.UserId
)

select uf.MessageId, uf.UserName [FromUser], ut.UserName [ToUser], uf.ModifiedMessage [Message], uf.TimeOfSending
from FromUserName uf
inner join ToUserName ut
on uf.MessageId = ut.MessageId

end

GO
/****** Object:  StoredProcedure [dbo].[GetNumberOfIdPhrases]    Script Date: 03.07.2016 21:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[GetNumberOfIdPhrases]
AS
BEGIN
select count(IdPhrase) from Phrases
END


GO
/****** Object:  StoredProcedure [dbo].[GetPhraseById]    Script Date: 03.07.2016 21:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[GetPhraseById]
		@currentId int
AS
BEGIN
select Phrase from Phrases
where IdPhrase = @currentId
END


GO
/****** Object:  StoredProcedure [dbo].[sp_CheckUserData]    Script Date: 03.07.2016 21:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_CheckUserData]
		@currentUserName varchar(50),
		@currentUserPassword varchar(50)
AS
BEGIN
	select UserId from [User]
	where UserName = @currentUserName and [Password] = CONVERT(VARCHAR(32), HashBytes('MD5', @currentUserPassword), 2) 
END


GO
/****** Object:  StoredProcedure [dbo].[sp_DeleteMessages]    Script Date: 03.07.2016 21:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_DeleteMessages]
			@currentUser int
As
BEGIN
	delete Message where FromUser = @currentUser or ToUser = @currentUser
END



GO
/****** Object:  StoredProcedure [dbo].[sp_GetConnections]    Script Date: 03.07.2016 21:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_GetConnections]
as
begin
	select FromUser, ToUser from [Message] 
	except 
	select ToUser, FromUser from [Message] 
	union 
	select distinct m1.FromUser, m1.ToUser from [Message] m1 
	inner join [Message] m2 on (m1.FromUser = m2.ToUser and m1.ToUser = m2.FromUser) 
	where m1.FromUser < m2.FromUser
end


GO
/****** Object:  StoredProcedure [dbo].[sp_GetInboxMessages]    Script Date: 03.07.2016 21:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_GetInboxMessages]
			@currentUser int
As
BEGIN
	select u.UserName, m.ModifiedMessage  
	from Message m 
	inner join [User] u
	on m.FromUser = u.UserId
	where ToUser = @currentUser
END


GO
/****** Object:  StoredProcedure [dbo].[sp_GetOutboxMessages]    Script Date: 03.07.2016 21:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[sp_GetOutboxMessages]
			@currentUser int
As
BEGIN
	select u.UserName, m.ModifiedMessage  
	from Message m 
	inner join [User] u
	on m.FromUser = u.UserId
	where ToUser = @currentUser
END


GO
/****** Object:  StoredProcedure [dbo].[sp_GetUser]    Script Date: 03.07.2016 21:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_GetUser]
		@currentUserName varchar(50)
AS
BEGIN
	select UserId, UserName, [Password] from [User]
	where UserName = @currentUserName 
END



GO
/****** Object:  StoredProcedure [dbo].[sp_GetUserById]    Script Date: 03.07.2016 21:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_GetUserById]
		@userId int
AS
BEGIN
	select UserId, UserName, [Password] from [User]
	where UserId = @userId 
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetUserDialogs]    Script Date: 03.07.2016 21:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[sp_GetUserDialogs]
      @userId int
AS
BEGIN
	;with groupFromTo as
	(
		select FromUser, ToUser, max(TimeOfSending) [MaxTime]
		from [Message]
		where FromUser = @userId
		group by FromUser, ToUser
	)
	,groupToFrom as
	(
		select FromUser, ToUser, max(TimeOfSending) [MaxTime]
		from [Message]
		where ToUser = @userId
		group by FromUser, ToUser
	)
	,grouped as
	(
		select m.FromUser, m.ToUser, TimeOfSending
		from [groupFromTo] gt
		inner join [Message] m
			on m.FromUser = gt.FromUser and m.ToUser = gt.ToUser and m.TimeOfSending = gt.MaxTime
		union
		select m.ToUser, m.FromUser, TimeOfSending
		from [groupToFrom] gf
		inner join [Message] m
			on m.FromUser = gf.FromUser and m.ToUser = gf.ToUser and m.TimeOfSending = gf.MaxTime
	)
	,distincted as
	(
		select FromUser, ToUser, max(TimeOfSending) [MaxTime]
		from [grouped]
		group by FromUser, ToUser
	)
	,lastMessages as
	(
		select g.MessageId [MessageId], g.FromUser [FromUser], g.ToUser [ToUser], g.ModifiedMessage [Message], g.TimeOfSending [TimeOfSending]
		from [Message] g
		inner join [distincted] d
			on g.TimeOfSending = d.MaxTime and 
		g.FromUser = d.FromUser and g.ToUser = d.ToUser
		union
		select g.MessageId [MessageId], g.FromUser [FromUser], g.ToUser [ToUser], g.ModifiedMessage [Message], g.TimeOfSending [TimeOfSending]
		from [Message] g
		inner join [distincted] d
			on g.TimeOfSending = d.MaxTime and 
		g.FromUser = d.ToUser and g.ToUser = d.FromUser
	)

	--change userId to usernames
	,FromUserName as
	(
		select d.MessageId, u.UserName, d.[Message], d.TimeOfSending
		from [lastMessages] d
		inner join [User] u
		on d.FromUser = u.UserId
	)
	,ToUserName as
	(
		select d.MessageId, u.UserName
		from [lastMessages] d
		inner join [User] u
		on d.ToUser = u.UserId
	)

	select g.MessageId [MessageId], uf.UserName [FromUser], ut.UserName [ToUser], g.ModifiedMessage [Message], g.TimeOfSending [TimeOfSending]
	from [Message] g
	inner join [lastMessages] d 
		on g.TimeOfSending = d.[TimeOfSending] and 
		g.FromUser = d.FromUser and g.ToUser = d.ToUser
	inner join FromUserName uf on d.MessageId = uf.MessageId
	inner join ToUserName ut on uf.MessageId = ut.MessageId			
	union
	select g.MessageId [MessageId], uf.UserName [FromUser], ut.UserName [ToUser], g.ModifiedMessage [Message], g.TimeOfSending [TimeOfSending]
	from [Message] g
	inner join [lastMessages] d 
		on g.TimeOfSending = d.[TimeOfSending] and 
		g.FromUser = d.FromUser and g.ToUser = d.ToUser
	inner join FromUserName uf on d.MessageId = uf.MessageId
	inner join ToUserName ut on uf.MessageId = ut.MessageId	
END

GO
/****** Object:  StoredProcedure [dbo].[sp_GetUsers]    Script Date: 03.07.2016 21:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_GetUsers]

AS
BEGIN
    SELECT * FROM [User]
END


GO
/****** Object:  StoredProcedure [dbo].[sp_GetUsersFriends]    Script Date: 03.07.2016 21:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_GetUsersFriends]
			@currentUser int
As
BEGIN
	select distinct u.UserId, u.UserName
	from Message m 
	inner join [User] u
	on m.ToUser = u.UserId
	where FromUser = @currentUser
	union
	select distinct u.UserId, u.UserName
	from Message m 
	inner join [User] u
	on m.FromUser = u.UserId
	where ToUser = @currentUser
END


GO
/****** Object:  StoredProcedure [dbo].[sp_InsertMessage]    Script Date: 03.07.2016 21:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[sp_InsertMessage]
      @FromUser int,
	  @ToUser int,
	  @originalMessage varchar(max),
	  @modifiedMessage varchar(max),
	  @timeOfSending datetime
AS
BEGIN
INSERT INTO [dbo].[Message]
           ([FromUser]
           ,[ToUser]
           ,[OriginalMessage]
           ,[ModifiedMessage]
		   ,[TimeOfSending])
     VALUES
           (@FromUser, @ToUser, @originalMessage, @modifiedMessage, @timeOfSending)
END



GO
/****** Object:  StoredProcedure [dbo].[sp_InsertUser]    Script Date: 03.07.2016 21:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[sp_InsertUser]
      @username varchar(50),
      @password varchar(50)
AS

BEGIN

    INSERT INTO [User] (UserName, Password)
    VALUES (@username, CONVERT(VARCHAR(32), HashBytes('MD5', @password), 2))

END


GO
/****** Object:  StoredProcedure [dbo].[sp_RemoveUser]    Script Date: 03.07.2016 21:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[sp_RemoveUser]
	  @userId int
      
AS

BEGIN

    DELETE from [User] 
    where @userId = UserId;
	
END


GO
/****** Object:  StoredProcedure [dbo].[sp_SearchUser]    Script Date: 03.07.2016 21:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_SearchUser]
		@predicate nvarchar(50)
AS
BEGIN
	select UserId, UserName
	from [User]
	where UserName like @predicate + '%'
END
GO
/****** Object:  StoredProcedure [dbo].[WhatIsAdd]    Script Date: 03.07.2016 21:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[WhatIsAdd]
		@ArticleName		nvarchar(50)
		,@ArticleParagraph	nvarchar(2000)
as
begin
	insert into Whatis
	(ArticleName, ArticleParagraph)
	values
	(@ArticleName, @ArticleParagraph)
end


GO
/****** Object:  StoredProcedure [dbo].[WhatIsGet]    Script Date: 03.07.2016 21:39:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[WhatIsGet]
		@ArticleName	nvarchar(50)
as
begin
	select ArticleName, ArticleParagraph
	from WhatIs
	where ArticleName = @ArticleName
end


GO
USE [master]
GO
ALTER DATABASE [DogeDB] SET  READ_WRITE 
GO
