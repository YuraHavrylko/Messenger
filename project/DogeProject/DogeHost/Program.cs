﻿using System;
using System.ServiceModel;
using DogeWebService.Presenters;

namespace DogeHost
{
    class Program
    {
        static void Main()
        {
            using (ServiceHost userHandlerHost = new ServiceHost(typeof(UserHandler)))
            using (ServiceHost userFirendshipHost = new ServiceHost(typeof(UserFriendship)))
            using (ServiceHost botHost = new ServiceHost(typeof(Bot)))
            using (ServiceHost chatHost = new ServiceHost(typeof(Chat)))
            {
                userHandlerHost.Open();
                userFirendshipHost.Open();
                botHost.Open();
                chatHost.Open();
                Console.WriteLine("Host started @ " + DateTime.Now);
                Console.ReadKey();
            }
        }
    }
}
