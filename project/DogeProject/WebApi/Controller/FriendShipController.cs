﻿using System;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using WebApi.UserFriendship;

namespace WebApi.Controller
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("api/FriendShip")]
    public class FriendShipController : ApiController
    {
        private readonly IUserFriendship _friendship;

        public FriendShipController()
        {
            _friendship = new UserFriendshipClient();
        }


        /**
        * @api {get} api/FriendShip/GetCommonFriends/:IdFriend Request Get common friend with user
        * @apiName GetCommonFriends
        * @apiGroup FriendShip
        *
        * @apiExample Example usage:
        *     http://localhost:49768/api/FriendShip/GetCommonFriends/32
        * @apiParam {number} idFriend Id user wit which get common friedns.
        * @apiHeaderExample {json} Header-Example:
        *     {
        *       "Content-Type": "application/json; charset=UTF-8",
        *	    "Authorization": "bearer MTrFwnJ7jHH-y..."
        *     }
        * @apiSuccessExample Success-Response:
        *     HTTP/1.1 200 OK
        *     [
        *       {
        *           "userId": 23,
        *           "userName": "SomeUser1"
        *       },
        *       {
        *           "userId": 56,
        *           "userName": "SomeUser2"
        *       }
        *     ]
        * @apiError NotAuthorized User not enter in system.
        *
        * @apiErrorExample NotAuthorized:
        *     HTTP/1.1 401 Unauthorized
        *     {
        *       "message":"Authorization has been denied for this request.",
        *     }
        *
        */
        [Authorize]
        [Route("GetCommonFriends/{IdFriend:int}")]
        public IHttpActionResult GetCommonFriends(int idFriend)
        {
            try
            {
                var users = _friendship.FindCommonFriends(UserHelper.GetCurrentUserId(Request.GetRequestContext()), idFriend);
                return Ok(users);
            }
            catch (Exception exception)
            {
                return InternalServerError(exception);
            }
        }

        /**
        * @api {get} api/FriendShip/GetCountPath/:IdFriend Request Get count path to user
        * @apiName GetCountPath
        * @apiGroup FriendShip
        *
        * @apiExample Example usage:
        *     http://localhost:49768/api/FriendShip/GetCountPath/5
        * @apiParam {number} idFriend Id user wit which get count path.
        * @apiHeaderExample {json} Header-Example:
        *     {
        *       "Content-Type": "application/json; charset=UTF-8",
        *	    "Authorization": "bearer MTrFwnJ7jHH-y..."
        *     }
        * @apiSuccessExample Success-Response:
        *     HTTP/1.1 200 OK
        *     4
        * @apiError NotAuthorized User not enter in system.
        *
        * @apiErrorExample NotAuthorized:
        *     HTTP/1.1 401 Unauthorized
        *     {
        *       "message":"Authorization has been denied for this request.",
        *     }
        *
        */
        [Authorize]
        [Route("GetCountPath/{IdFriend:int}")]
        public IHttpActionResult GetCountPath(int idFriend)
        {
            try
            {
                var count = _friendship.FindPath(UserHelper.GetCurrentUserId(Request.GetRequestContext()), idFriend);
                return Ok(count);
            }
            catch (Exception exception)
            {
                return InternalServerError(exception);
            }
        }


        /**
        * @api {get} api/FriendShip/GetCurrentUserFriends Request Get current user friends
        * @apiName GetCurrentUserFriends
        * @apiGroup FriendShip
        *
        * @apiExample Example usage:
        *     http://localhost:49768/api/FriendShip/GetCurrentUserFriends
        * @apiHeaderExample {json} Header-Example:
        *     {
        *       "Content-Type": "application/json; charset=UTF-8",
        *	    "Authorization": "bearer MTrFwnJ7jHH-y..."
        *     }
        * @apiSuccessExample Success-Response:
        *     HTTP/1.1 200 OK
        *     [
        *       {
        *           "userId": 23,
        *           "userName": "SomeUser1"
        *       },
        *       {
        *           "userId": 56,
        *           "userName": "SomeUser2"
        *       },
        *     ]
        * @apiError NotAuthorized User not enter in system.
        *
        * @apiErrorExample NotAuthorized:
        *     HTTP/1.1 401 Unauthorized
        *     {
        *       "message":"Authorization has been denied for this request.",
        *     }
        *
        */
        // Get api/FriendShip/GetCurrentUserFriends
        [Authorize]
        [Route("GetCurrentUserFriends")]
        public IHttpActionResult GetCurrentUserFriends()
        {
            try
            {
                var friends = _friendship.GetUsersFriends(UserHelper.GetCurrentUserId(Request.GetRequestContext()));
                return Ok(friends);
            }
            catch (Exception exception)
            {
                return InternalServerError(exception);
            }
        }

        /**
        * @api {get} api/FriendShip/GetUserFriends/:IdFriend Request Get friends of user
        * @apiName GetUserFriends
        * @apiGroup FriendShip
        *
        * @apiExample Example usage:
        *     http://localhost:49768/api/FriendShip/GetUserFriends/46
        * @apiParam {number} idUser Id user wit which get friends.
        * @apiHeaderExample {json} Header-Example:
        *     {
        *       "Content-Type": "application/json; charset=UTF-8",
        *	    "Authorization": "bearer MTrFwnJ7jHH-y..."
        *     }
        * @apiSuccessExample Success-Response:
        *     HTTP/1.1 200 OK
        *     [
        *       {
        *           "userId": 23,
        *           "userName": "SomeUser1"
        *       },
        *       {
        *           "userId": 56,
        *           "userName": "SomeUser2"
        *       },
        *     ]
        * @apiError NotAuthorized User not enter in system.
        *
        * @apiErrorExample NotAuthorized:
        *     HTTP/1.1 401 Unauthorized
        *     {
        *       "message":"Authorization has been denied for this request.",
        *     }
        *
        */
        [Authorize]
        [Route("GetUserFriends/{IdUser:int}")]
        public IHttpActionResult GetUserFriends(int idUser)
        {
            try
            {
                var friends = _friendship.GetUsersFriends(idUser);
                return Ok(friends);
            }
            catch (Exception exception)
            {
                return InternalServerError(exception);
            }
        }

        /**
        * @api {get} api/FriendShip/GetCountAndFriendsPath/:IdFriend Request Get count path and result data to user
        * @apiName GetCountAndFriendsPath
        * @apiGroup FriendShip
        *
        * @apiExample Example usage:
        *     http://localhost:49768/api/FriendShip/GetCountAndFriendsPath/41
        * @apiParam {number} idUser Id user wit which get path and result data.
        * @apiHeaderExample {json} Header-Example:
        *     {
        *       "Content-Type": "application/json; charset=UTF-8",
        *	    "Authorization": "bearer MTrFwnJ7jHH-y..."
        *     }
        * @apiSuccessExample Success-Response:
        *     HTTP/1.1 200 OK
        *     [
        *       {
        *           "userId": 23,
        *           "userName": "SomeUser1"
        *       },
        *       {
        *           "userId": 56,
        *           "userName": "SomeUser2"
        *       },
        *     ]
        * @apiError NotAuthorized User not enter in system.
        *
        * @apiErrorExample NotAuthorized:
        *     HTTP/1.1 401 Unauthorized
        *     {
        *       "message":"Authorization has been denied for this request.",
        *     }
        *
        */
        [Authorize]
        [Route("GetCountAndFriendsPath/{IdUser:int}")]
        public IHttpActionResult GetCountAndFriendsPath(int idUser)
        {
            try
            {
                var resultData = _friendship.FindPathWithResultData(UserHelper.GetCurrentUserId(Request.GetRequestContext()), idUser);
                return Ok(resultData);
            }
            catch (Exception exception)
            {
                return InternalServerError(exception);
            }
        }
    }
}
