﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using Microsoft.AspNet.Identity;
using WebApi.Models;

namespace WebApi.Controller
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("api/Account")]
    public class AccountController : ApiController
    {
        private CustomUserManager _customUserManager = null;

        public AccountController()
        {
            _customUserManager = new CustomUserManager(new CustomUserStore());
        }


        /**
        * @api {post} api/Account/Token Request Login user
        * @apiName Login
        * @apiGroup Account
        *
        * @apiParam {String} grant_type Type of value which get token
        * @apiParam {String} username Username user which login.
        * @apiParam {String} password Password name which login.
        *
        * @apiParamExample {x-www-form-urlencoded} Request-Example:
        * {
        *   grant_type=password&username=Someuser&password=Somepassword
        * }
        *
        * @apiHeaderExample {json} Header-Example:
        *     {
        *       "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8"
        *     }
        * @apiSuccessExample Success-Response:
        *     HTTP/1.1 200 OK
        *     {
        *       "access_token":"EDB3VUWC9...",
        *       "token_type":"bearer",
        *       "expires_in":86399,
        *       "refresh_token":"w4lKg0M..."
        *     }
        *
        * @apiError UserNotExist The user with this username and password don't exists.
        *
        * @apiErrorExample UserNotExist:
        *     HTTP/1.1 400 Bad Request
        *     {
        *       "error":"invalid_grant",
        *       "error_description":"User with that name and password does not exist"
        *     }
        *
        */



        /**
        * @api {post} api/Account/SignUp Request Register user
        * @apiName SignUp
        * @apiGroup Account
        *
        * @apiParam {String} UserName User unique username.
        * @apiParam {String} Password User password.
        *
        * @apiParamExample {json} Request-Example:
        *     {
        *       "UserName": "Someuser",
        *       "Password": "Somepassword"
        *     }
        *
        * @apiHeaderExample {json} Header-Example:
        *     {
        *       "Content-Type": "application/json; charset=UTF-8"
        *     }
        * @apiSuccessExample Success-Response:
        *     HTTP/1.1 200 OK
        *     "You successfully registered" 
        *
        * @apiError UserAlreadyExist The user with this username already exists.
        *
        * @apiErrorExample UserExists:
        *     HTTP/1.1 500 Internal Server Error
        *     {
        *       "message":"An error has occurred.",
        *       "exceptionMessage":"User with that username {username} is already exist",
        *       "exceptionType":"System.Exception",
        *       "stackTrace":null
        *     }
        *
        * @apiErrorExample UserNameLength:
        *     HTTP/1.1 500 Internal Server Error
        *     {
        *       "message":"An error has occurred.",
        *       "exceptionMessage":"Username length should not be less than 6 characters and more than 20\nPassword should be of length 6",
        *       "exceptionType":"System.Exception",
        *       "stackTrace":null
        *     }
        *
        * @apiErrorExample PasswordLength:
        *     HTTP/1.1 500 Internal Server Error
        *     {
        *       "message":"An error has occurred.",
        *       "exceptionMessage":"Password should be of length 6",
        *       "exceptionType":"System.Exception",
        *       "stackTrace":null
        *     }
        */
        [AllowAnonymous]
        [Route("SignUp")]
        public async Task<IHttpActionResult> SignUp(User user)
        {
            //var user = request.Content.ReadAsAsync<User>().Result;
            IdentityResult result = await _customUserManager.CreateAsync(new CustomUser(user.UserName) { Password = user.Password }, user.Password);
            if (result.Succeeded)
            {
                return Ok("You successfully registered");
            }
            return InternalServerError(new Exception(String.Join("\n", result.Errors.ToList())));
        }


        /**
        * @api {delete} api/Account/DeleteAccount Request Remove user
        * @apiName Delete
        * @apiGroup Account
        *
        * @apiHeaderExample {json} Header-Example:
        *     {
        *       "Content-Type": "application/json; charset=UTF-8"
	    *	    "Authorization": "bearer MTrFwnJ7jHH-y..."
        *     }
        * @apiSuccessExample Success-Response:
        *     HTTP/1.1 200 OK
        *     "User deleted" 
        *
        * @apiError NotAuthorized User not enter in system.
        *
        * @apiErrorExample NotAuthorized:
        *     HTTP/1.1 401 Unauthorized
        *     {
        *       "message":"Authorization has been denied for this request.",
        *     }
        *
        */
        [Authorize]
        [Route("DeleteAccount")]
        public async Task<IHttpActionResult> DeleteAccount()
        {
            IdentityResult result = await _customUserManager.DeleteAsync(new CustomUser(User.Identity.Name));
            if (result.Succeeded)
            {
                return Ok("User deleted");
            }
            return InternalServerError(new Exception(String.Join("\n", result.Errors.ToList())));
        }
    }
}
