﻿using System;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using WebApi.Bot;
using WebApi.Chat;
using WebApi.WebServise;
using Message = WebApi.Models.Message;

namespace WebApi.Controller
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("api/Message")]
    public class MessageController : ApiController
    {
        private readonly IChat _chat;
        private readonly IBot _bot;
        private readonly IUserHandler _userHandler;

        public MessageController()
        {
            _chat = new ChatClient();
            _userHandler = new UserHandlerClient();
            _bot = new BotClient();
        }


        /**
        * @api {post} api/Message/SendMessage Request Post message to other user
        * @apiName SendMessage
        * @apiGroup Message
        * @apiParam {string} fromUser UserName of user from message.
        * @apiParam {string} toUser UserName of user to message.
        * @apiParam {string} message Test message.
        * @apiParamExample {text} Request-Example:
        *   {
        *       "fromUser": "Someuser1",
        *       "toUser": "Someuser2",   
        *       "message": "Hello",
        *   }
        * @apiExample Example usage:
        *     http://localhost:49768/api/Message/SendMessage
        * @apiHeaderExample {text} Header-Example:
        *     {
        *       "Content-Type": "application/json; charset=UTF-8",
        *	    "Authorization": "bearer MTrFwnJ7jHH-y..."
        *     }
        * @apiSuccessExample Success-Response:
        *     HTTP/1.1 200 OK
        *       
        * @apiError NotAuthorized User not enter in system.
        *
        * @apiErrorExample NotAuthorized:
        *     HTTP/1.1 401 Unauthorized
        *     {
        *       "message":"Authorization has been denied for this request.",
        *     }
        *
        */
        [Authorize]
        [Route("SendMessage")]
        public IHttpActionResult SendMessage(Message message)
        {
            try
            {
                int currentUserId = UserHelper.GetCurrentUserId(Request.GetRequestContext());
                int idFriend = GetFriendId(message.fromUser, message.toUser);

                _chat.WriteMessage(currentUserId, idFriend, message.message);
                return Ok();
            }
            catch (Exception exception)
            {
                return InternalServerError(exception);
            }
        }

        /**
        * @api {get} api/Message/GetDialog:fromUser/:toUser Request Get dialog between users
        * @apiName GetDialog
        * @apiGroup Message
        *
        * @apiParam {string} fromUser UserName of user from message.
        * @apiParam {string} toUser UserName of user to message.
        * @apiExample Example usage:
        *     http://localhost:49768/api/Message/GetDialog/someuser1/someuser2
        * @apiHeaderExample {json} Header-Example:
        *     {
        *       "Content-Type": "application/json; charset=UTF-8",
        *	    "Authorization": "bearer MTrFwnJ7jHH-y..."
        *     }
        * @apiSuccessExample Success-Response:
        *     HTTP/1.1 200 OK
        *       [
        *           {
        *               "messageId": 23,
        *               "fromUser": "SomeUser1",
        *               "toUser": "Someuser2",
        *               "message": "Hello my friend",
        *               "timeOfSending": "2016-05-25T13:10:00"
        *           },
        *           {
        *               "messageId": 29,
        *               "fromUser": "SomeUser5",
        *               "toUser": "Someuser3",
        *               "message": "How are you",
        *               "timeOfSending": "2016-05-25T13:10:00"
        *           }
        *       ]
        *       
        * @apiError NotAuthorized User not enter in system.
        *
        * @apiErrorExample NotAuthorized:
        *     HTTP/1.1 401 Unauthorized
        *     {
        *       "message":"Authorization has been denied for this request.",
        *     }
        *
        */
        [Authorize]
        [Route("GetDialog/{fromUser}/{toUser}")]
        public IHttpActionResult GetDialog(string fromUser, string toUser)
        {
            try
            {
                int currentUserId = UserHelper.GetCurrentUserId(Request.GetRequestContext());
                int idFriend = GetFriendId(fromUser, toUser);

                return Ok(_chat.GetDialog(currentUserId, idFriend));
            }
            catch (Exception exception)
            {
                return InternalServerError(exception);
            }
        }

        /**
        * @api {get} api/Message/GetDialogFromDate:fromUser/:toUser/:fromDate Request Get dialog between users from date
        * @apiName GetDialogFromDate
        * @apiGroup Message
        *
        * @apiParam {string} fromUser UserName of user from message.
        * @apiParam {string} toUser UserName of user to message.
        * @apiParam {number} fromDate Time to get messages.
        * @apiExample Example usage:
        *     http://localhost:49768/api/Message/GetDialogFromDate/someuser1/someuser2/23657285635
        * @apiHeaderExample {json} Header-Example:
        *     {
        *       "Content-Type": "application/json; charset=UTF-8",
        *	    "Authorization": "bearer MTrFwnJ7jHH-y..."
        *     }
        * @apiSuccessExample Success-Response:
        *     HTTP/1.1 200 OK
        *       [
        *           {
        *               "messageId": 23,
        *               "fromUser": "SomeUser1",
        *               "toUser": "Someuser2",
        *               "message": "Hello my friend",
        *               "timeOfSending": "2016-05-25T13:10:00"
        *           },
        *           {
        *               "messageId": 29,
        *               "fromUser": "SomeUser5",
        *               "toUser": "Someuser3",
        *               "message": "How are you",
        *               "timeOfSending": "2016-05-25T13:10:00"
        *           }
        *       ]
        *       
        * @apiError NotAuthorized User not enter in system.
        *
        * @apiErrorExample NotAuthorized:
        *     HTTP/1.1 401 Unauthorized
        *     {
        *       "message":"Authorization has been denied for this request.",
        *     }
        *
        */
        [Authorize]
        [Route("GetDialogFromDate/{fromUser}/{toUser}/{fromDate}")]
        public IHttpActionResult GetDialogFromDate(string fromUser, string toUser, int fromDate)
        {
            try
            {
                int currentUserId = UserHelper.GetCurrentUserId(Request.GetRequestContext());
                int idFriend = GetFriendId(fromUser, toUser);

                return Ok(_chat.GetDialogFromDate(currentUserId, idFriend, fromDate));
            }
            catch (Exception exception)
            {
                return InternalServerError(exception);
            }
        }

        /**
        * @api {get} api/Message/GetCurrentDialogs Request Get cuurent user dialog
        * @apiName GetCurrentDialogs
        * @apiGroup Message
        *
        * @apiExample Example usage:
        *     http://localhost:49768/api/Message/GetCurrentDialogs
        * @apiHeaderExample {json} Header-Example:
        *     {
        *       "Content-Type": "application/json; charset=UTF-8",
        *	    "Authorization": "bearer MTrFwnJ7jHH-y..."
        *     }
        * @apiSuccessExample Success-Response:
        *     HTTP/1.1 200 OK
        *       [
        *           {
        *               "messageId": 23,
        *               "fromUser": "SomeUser1",
        *               "toUser": "Someuser2",
        *               "message": "Hello my friend",
        *               "timeOfSending": "2016-05-25T13:10:00"
        *           },
        *           {
        *               "messageId": 29,
        *               "fromUser": "SomeUser5",
        *               "toUser": "Someuser3",
        *               "message": "How are you",
        *               "timeOfSending": "2016-05-25T13:10:00"
        *           }
        *       ]
        *       
        * @apiError NotAuthorized User not enter in system.
        *
        * @apiErrorExample NotAuthorized:
        *     HTTP/1.1 401 Unauthorized
        *     {
        *       "message":"Authorization has been denied for this request.",
        *     }
        *
        */
        [Authorize]
        [Route("GetCurrentDialogs")]
        public IHttpActionResult GetCurrentDialogs()
        {
            try
            {
                return Ok(_chat.GetAllUserDialogs(UserHelper.GetCurrentUserId(Request.GetRequestContext())));
            }
            catch (Exception exception)
            {
                return InternalServerError(exception);
            }
        }

        /**
        * @api {post} api/Message/SendBotMessage Request Post message to bot
        * @apiName SendBotMessage
        * @apiGroup Message
        * @apiParam {String} Message User message.
        *
        * @apiParamExample {text} Request-Example:
        *     {
        *        hello
        *     }
        * @apiExample Example usage:
        *     http://localhost:49768/api/Message/SendBotMessage
        * @apiHeaderExample {text} Header-Example:
        *     {
        *       "Content-Type": "application/json; charset=UTF-8",
        *	    "Authorization": "bearer MTrFwnJ7jHH-y..."
        *     }
        * @apiSuccessExample Success-Response:
        * HTTP/1.1 200 OK
        * {
        *   "There used to be a street named after Chuck Norris, but it was changed because nobody crosses Chuck Norris and lives"
        * }
        *       
        * @apiError NotAuthorized User not enter in system.
        *
        * @apiErrorExample NotAuthorized:
        * HTTP/1.1 401 Unauthorized
        * {
        *   "message":"Authorization has been denied for this request.",
        * }
        *
        */
        [Authorize]
        [Route("SendBotMessage")]
        public IHttpActionResult SendBotMessage(HttpRequestMessage text)
        {
            try
            {
                var message = text.Content.ReadAsStringAsync().Result;
                return Ok(_bot.MessageHandler(message));
            }
            catch (Exception exception)
            {
                return InternalServerError(exception);
            }
        }

        private int GetFriendId(string fromUser, string toUser)
        {
            int currentUserId = UserHelper.GetCurrentUserId(Request.GetRequestContext());
            int fromUserId = _userHandler.GetUserByUserName(fromUser).userId;
            int toUserId = _userHandler.GetUserByUserName(toUser).userId;

            return currentUserId == fromUserId ? toUserId : fromUserId;
        }
    }
}
