﻿using System;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using WebApi.WebServise;

namespace WebApi.Controller
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("api/User")]
    public class UserController : ApiController
    {
        private readonly IUserHandler _handler;


        public UserController()
        {
            _handler = new UserHandlerClient();
        }

        /**
        * @api {get} api/Users/GetUsers Request Get users
        * @apiName GetUsers
        * @apiGroup User
        *
        * @apiExample Example usage:
        *     http://localhost:49768/api/User/GetUsers
        * @apiHeaderExample {json} Header-Example:
        *     {
        *       "Content-Type": "application/json; charset=UTF-8",
        *	    "Authorization": "bearer MTrFwnJ7jHH-y..."
        *     }
        * @apiSuccessExample Success-Response:
        *     HTTP/1.1 200 OK
        *     [
        *       {
        *           "userId": 23,
        *           "userName": "SomeUser1"
        *       },
        *       {
        *           "userId": 56,
        *           "userName": "SomeUser2"
        *       },
        *     ]
        * @apiError NotAuthorized User not enter in system.
        *
        * @apiErrorExample NotAuthorized:
        *     HTTP/1.1 401 Unauthorized
        *     {
        *       "message":"Authorization has been denied for this request.",
        *     }
        *
        */

        [Authorize]
        [Route("GetUsers")]
        public IHttpActionResult GetUsers()
        {
            try
            {
                var users = _handler.GetUsers();
                return Ok(users);
            }
            catch (Exception exception)
            {
                return InternalServerError(exception);
            }
        }

        /**
        * @api {get} api/Users/GetCurrentUser Request Get current user
        * @apiName GetCurrentUser
        * @apiGroup User
        *
        * @apiExample Example usage:
        *     http://localhost:49768/api/User/GetCurrentUser
        * @apiHeaderExample {json} Header-Example:
        *     {
        *       "Content-Type": "application/json; charset=UTF-8",
        *	    "Authorization": "bearer MTrFwnJ7jHH-y..."
        *     }
        * @apiSuccessExample Success-Response:
        *     HTTP/1.1 200 OK
        *       {
        *           "userId": 23,
        *           "userName": "SomeUser1"
        *       }
        *       
        * @apiError NotAuthorized User not enter in system.
        *
        * @apiErrorExample NotAuthorized:
        *     HTTP/1.1 401 Unauthorized
        *     {
        *       "message":"Authorization has been denied for this request.",
        *     }
        *
        */
        [Authorize]
        [Route("GetCurrentUser")]
        public IHttpActionResult GetCurrentUser()
        {
            try
            {
                var user = _handler.GetUserById(UserHelper.GetCurrentUserId(Request.GetRequestContext()));
                return Ok(user);
            }
            catch (Exception exception)
            {
                return InternalServerError(exception);
            }
        }

        /**
        * @api {get} api/Users/GetUser/:idUser Request Get user by id
        * @apiName GetUser
        * @apiGroup User
        * @apiParam {number} idUser Id user to get.
        * @apiExample Example usage:
        *     http://localhost:49768/api/User/GetUser/23
        * @apiHeaderExample {json} Header-Example:
        *     {
        *       "Content-Type": "application/json; charset=UTF-8",
        *	    "Authorization": "bearer MTrFwnJ7jHH-y..."
        *     }
        * @apiSuccessExample Success-Response:
        *     HTTP/1.1 200 OK
        *       {
        *           "userId": 23,
        *           "userName": "SomeUser1"
        *       }
        *       
        * @apiError NotAuthorized User not enter in system.
        *
        * @apiErrorExample NotAuthorized:
        *     HTTP/1.1 401 Unauthorized
        *     {
        *       "message":"Authorization has been denied for this request.",
        *     }
        *
        */

        [Authorize]
        [Route("GetUser/{idUser:int}")]
        public IHttpActionResult GetUser(int idUser)
        {
            try
            {
                var user = _handler.GetUserById(idUser);
                return Ok(user);
            }
            catch (Exception exception)
            {
                return InternalServerError(exception);
            }
        }

        /**
       * @api {get} api/Users/GetUserByUserName/:username Request Get user by username
       * @apiName GetUserByUserName
       * @apiGroup User
       * @apiParam {string} UserName Username user to get.
       * @apiExample Example usage:
       *     http://localhost:49768/api/User/GetUserByUserName/someuser
       * @apiHeaderExample {json} Header-Example:
       *     {
       *       "Content-Type": "application/json; charset=UTF-8",
       *	    "Authorization": "bearer MTrFwnJ7jHH-y..."
       *     }
       * @apiSuccessExample Success-Response:
       *     HTTP/1.1 200 OK
       *       {
       *           "userId": 23,
       *           "userName": "someuser"
       *       }
       *       
       * @apiError NotAuthorized User not enter in system.
       *
       * @apiErrorExample NotAuthorized:
       *     HTTP/1.1 401 Unauthorized
       *     {
       *       "message":"Authorization has been denied for this request.",
       *     }
       *
       */

        [Authorize]
        [Route("GetUserByUserName/{username}")]
        public IHttpActionResult GetUserByUserName(string username)
        {
            try
            {
                var user = _handler.GetUserByUserName(username);
                return Ok(user);
            }
            catch (Exception exception)
            {
                return InternalServerError(exception);
            }
        }

        // Get api/User/GetSearchedUsers
        [Authorize]
        [Route("GetSearchedUsers/{username}")]
        public IHttpActionResult GetSearchedUsers(string username)
        {
            try
            {
                var users = _handler.SearchUser(username);
                return Ok(users);
            }
            catch (Exception exception)
            {
                return InternalServerError(exception);
            }
        }
    }
}
