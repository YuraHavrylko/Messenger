﻿using System;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Http.Controllers;

namespace WebApi
{
    public static class UserHelper
    {
        public static int GetCurrentUserId(HttpRequestContext context)
        {
            ClaimsPrincipal principal = context.Principal as ClaimsPrincipal;
            return Convert.ToInt32(principal.Claims.Single(c => c.Type == "Id").Value);
        }

        public static string GetCurrentUserName(HttpRequestContext context)
        {
            return HttpContext.Current.User.Identity.Name;
        }
    }
}