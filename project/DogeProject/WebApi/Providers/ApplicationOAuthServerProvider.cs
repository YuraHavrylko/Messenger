using System;
using System.Threading.Tasks;
using Microsoft.Owin.Security.OAuth;
using System.Security.Claims;
using Microsoft.Owin;
using WebApi.Models;

namespace WebApi.Providers
{
    public class ApplicationOAuthServerProvider : OAuthAuthorizationServerProvider
    {
        private CustomUserManager _userManager;

        public ApplicationOAuthServerProvider()
        {
            _userManager = new CustomUserManager(new CustomUserStore());
        }

        public override Task MatchEndpoint(OAuthMatchEndpointContext context)
        {
            SetCORSPolicy(context.OwinContext);
            if (context.Request.Method == "OPTIONS")
            {
                context.RequestCompleted();
                return Task.FromResult(0);
            }

            return base.MatchEndpoint(context);
        }

        private void SetCORSPolicy(IOwinContext context)
        {
            context.Response.Headers.Add("Access-Control-Allow-Origin",
                                                    new string[] { "*" });
            context.Response.Headers.Add("Access-Control-Allow-Headers",
                                   new string[] { "Authorization", "Content-Type" });
            context.Response.Headers.Add("Access-Control-Allow-Methods",
                                   new string[] { "OPTIONS", "POST" });

        }

        public override async Task ValidateClientAuthentication(
            OAuthValidateClientAuthenticationContext context)
        {
            // This call is required...
            // but we're not using client authentication, so validate and move on...
            await Task.FromResult(context.Validated());
        }


        public override async Task GrantResourceOwnerCredentials(
            OAuthGrantResourceOwnerCredentialsContext context)
        {
            try
            {
                var user = await _userManager.FindAsync(context.UserName, context.Password);
                var identity = new ClaimsIdentity(context.Options.AuthenticationType);
                identity.AddClaim(new Claim(ClaimTypes.Name, context.UserName));
                identity.AddClaim(new Claim("Id", user.Id, ClaimValueTypes.Integer));
                context.Validated(identity);
            }
            catch (ArgumentException exception)
            {
                context.SetError("invalid_grant", exception.Message);
            }
        }
    }
}