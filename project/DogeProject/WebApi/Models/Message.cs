﻿namespace WebApi.Models
{
    public class Message
    {
        public string fromUser { get; set; }
        public string toUser { get; set; }
        public string message { get; set; }
    }
}