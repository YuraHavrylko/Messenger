﻿using Microsoft.AspNet.Identity;

namespace WebApi.Models
{
    class CustomUser : IUser
    {
        public CustomUser(string name)
        {
            UserName = name;
        }

        public string Id { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}