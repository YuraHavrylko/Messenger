﻿using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using static System.String;

namespace WebApi.Models.Validators
{
    public class CustomPasswordValidator : IIdentityValidator<string>
    {

        public int RequiredLength { get; set; }

        public CustomPasswordValidator(int length)
        {
            RequiredLength = length;
        }

        public Task<IdentityResult> ValidateAsync(string item)
        {
            if (IsNullOrEmpty(item) || item.Length < RequiredLength)
            {
                return
                    Task.FromResult(
                        IdentityResult.Failed($"Password should be of length {RequiredLength}"));

            }

            return Task.FromResult(IdentityResult.Success);
        }

        public Task<IdentityResult> ValidateConfirmAsync(string password, string confirmPassword)
        {
            if (password != confirmPassword)
            {
                return
                    Task.FromResult(
                        IdentityResult.Failed("Password don't equals confirm password"));

            }
            IdentityResult result = ValidateAsync(password).Result;
            if (!result.Succeeded)
            {
                return Task.FromResult(result);
            }

            return Task.FromResult(IdentityResult.Success);
        }
    }
}