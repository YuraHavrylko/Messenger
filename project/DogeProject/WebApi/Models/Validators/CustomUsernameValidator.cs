﻿using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using WebApi.WebServise;
using static System.String;

namespace WebApi.Models.Validators
{
    public class CustomUsernameValidator : IIdentityValidator<string>
    {
        public int MinLength { get; set; }
        public int MaxLength { get; set; }
        private readonly IUserHandler _userHandler;

        public CustomUsernameValidator(int minLength, int maxLength)
        {
            MinLength = minLength;
            MaxLength = maxLength;
            _userHandler = new UserHandlerClient();
        }

        public Task<IdentityResult> ValidateAsync(string item)
        {
            if (IsNullOrEmpty(item) || item.Length < MinLength || item.Length > MaxLength)
            {
                return
                    Task.FromResult(
                        IdentityResult.Failed(
                            $"Username length should not be less than {MinLength} characters and more than {MaxLength}"));

            }
            if (_userHandler.CheckUserExistence(item))
            {
                return Task.FromResult(IdentityResult.Failed($"User with that username '{item}' is already exist"));
            }
            return Task.FromResult(IdentityResult.Success);
        }

        public Task<IdentityResult> ValidateExistenceAsync(string item)
        {
            if (!_userHandler.CheckUserExistence(item))
            {
                return Task.FromResult(IdentityResult.Failed($"User with that name '{item}' does not exist"));
            }
            return Task.FromResult(IdentityResult.Success);
        }

        public Task<IdentityResult> ValidateUserAsync(string userName, string password)
        {
            if (!_userHandler.Login(userName, password))
            {
                return Task.FromResult(IdentityResult.Failed("User with that name and password does not exist"));
            }
            return Task.FromResult(IdentityResult.Success);
        }
    }
}