﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using WebApi.Models.Validators;

namespace WebApi.Models
{
    class CustomUserManager : UserManager<CustomUser>
    {
        private readonly CustomUserStore _userStore;
        private readonly CustomPasswordValidator _passwordValidator;
        private readonly CustomUsernameValidator _usernameValidator;
        public CustomUserManager(IUserStore<CustomUser> store) : base(store)
        {
            _passwordValidator = new CustomPasswordValidator(6);
            _userStore = new CustomUserStore();
            _usernameValidator = new CustomUsernameValidator(6, 20);
        }

        public override async Task<IdentityResult> CreateAsync(CustomUser user, string repassword)
        {
            IdentityResult validationPasswordResult = await _passwordValidator.ValidateConfirmAsync(user.Password, repassword);
            IdentityResult validationUsernameresult = _usernameValidator.ValidateAsync(user.UserName).Result;
            if (validationPasswordResult.Succeeded && validationUsernameresult.Succeeded)
            {
                    await _userStore.CreateAsync(user);
                    return IdentityResult.Success;
            }
            List<string> errors = validationUsernameresult.Errors.ToList();
            errors.AddRange(validationPasswordResult.Errors.ToList());
            return IdentityResult.Failed(errors.ToArray());
        }

        public override Task<CustomUser> FindAsync(string userName, string password)
        {
            IdentityResult result = _usernameValidator.ValidateUserAsync(userName, password).Result;
            if (result.Succeeded)
            {
                return Task<CustomUser>.Factory.StartNew(() => _userStore.FindByNameAsync(userName).Result);
            }
            throw new ArgumentException(String.Join("\n", result.Errors.ToArray()));
        }

        public override async Task<CustomUser> FindByNameAsync(string userName)
        {
            IdentityResult result = _usernameValidator.ValidateExistenceAsync(userName).Result;
            if (result.Succeeded)
            {
                return await _userStore.FindByNameAsync(userName);
            }
            throw new ArgumentException(String.Join("\n", result.Errors.ToList()));
        }

        public override async Task<IdentityResult> DeleteAsync(CustomUser user)
        {
            IdentityResult result = _usernameValidator.ValidateExistenceAsync(user.UserName).Result;
            if (result.Succeeded)
            {
                await _userStore.DeleteAsync(user);
                return IdentityResult.Success;
            }
            return IdentityResult.Failed(result.Errors.ToArray());
        }
    }
}
