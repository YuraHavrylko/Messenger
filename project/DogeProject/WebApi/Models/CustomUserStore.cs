﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using WebApi.WebServise;

namespace WebApi.Models
{
    class CustomUserStore : IUserStore<CustomUser>

    {
        private readonly IUserHandler _userHandler;

        public CustomUserStore()
        {
            _userHandler = new UserHandlerClient();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public Task CreateAsync(CustomUser user)
        {
            return Task.Factory.StartNew(() =>
            {
                _userHandler.AddUser(user.UserName, user.Password);
            });
        }


        public Task UpdateAsync(CustomUser user)
        {
            throw new NotImplementedException();
            //return Task.Factory.StartNew(() => _userHandler.UpdateUser(user.UserName, user.UserName, user.Password));
        }

        public Task DeleteAsync(CustomUser user)
        {
            return Task.Factory.StartNew(() => _userHandler.DeleteUser(Convert.ToInt32(user.Id)));
        }

        public Task<CustomUser> FindByIdAsync(string userId)
        {
            return Task.Factory.StartNew(() => new CustomUser(_userHandler.GetUserById(Convert.ToInt32(userId)).userName));
        }

        public Task<CustomUser> FindByNameAsync(string userName)
        {
            var user = _userHandler.GetUserByUserName(userName);
            return Task.Factory.StartNew(() => new CustomUser(user.userName) {Id = Convert.ToString(user.userId)});
        }
    }
}