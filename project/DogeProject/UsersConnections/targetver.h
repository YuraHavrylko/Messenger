#pragma once

// Including SDKDDKVer.h defines the highest available Windows platform.

// If you wish to build your application for a previous Windows platform, include WinSDKVer.h and
// set the _WIN32_WINNT macro to the platform you wish to support before including SDKDDKVer.h.
#ifdef MATHFUNCSDLL_EXPORTS
#define MATHFUNCSDLL_API __declspec(dllexport) 
#else
#define MATHFUNCSDLL_API __declspec(dllimport) 
#endif
#include <SDKDDKVer.h>
#include <iostream>
#include <cstdlib>
#include "stdafx.h"

struct ResultData;
struct EndpointData;

extern "C"
{
	__declspec(dllexport) int * __stdcall getCommonFriends(EndpointData* friendShips, int length, int idUser1, int idUser2, int & lengthResult);
	__declspec(dllexport) int __stdcall findPath(EndpointData* pStructs, int nItems, int userId1, int userId2);
	__declspec(dllexport) ResultData * __stdcall findPathWithResultData(EndpointData* pStructs, int nItems, int userId1, int userId2, int& resultArraySize);
}
