#pragma once
#ifdef MATHFUNCSDLL_EXPORTS
#define MATHFUNCSDLL_API __declspec(dllexport) 
#else
#define MATHFUNCSDLL_API __declspec(dllimport) 
#endif
#include <iostream>
#include <cstdlib>
#include "stdafx.h"
#include <vector>
#include <objbase.h>
#include <stdio.h>

extern "C" {

	struct EndpointData
	{
		int userId1;
		int userId2;
	};

	struct ResultData
	{
		int userNumberInPath;
		int userId;
	};

	std::vector<ResultData>* minPath;
	std::vector<ResultData>* currentPath;
	int minLevel = 0;
	int globalCurrentLevel = 0;

	void countLevel(EndpointData* pStructs, int nItems, int userId1, int userId2)
	{
		if (pStructs == nullptr)
		{
			return;
		}

		EndpointData* nextLevel = new EndpointData[nItems];             // use this array to hold connections that don't contain userName1
		int* currentLevelUsers = new int[nItems];                   // use this array to hold users that are directly connected with userName1
		int nextLevelArrSize = 0;
		int currentLevelUsersArrSize = 0;
		int currentLevel = globalCurrentLevel + 1;
		for (int i = 0; i < nItems; i++)
		{
			if (pStructs[i].userId1 == userId1 && pStructs[i].userId2 == userId2 ||   // check if we've found a connection
				pStructs[i].userId2 == userId1 && pStructs[i].userId1 == userId2)     // between userName1 and userName2
			{
				if (currentLevel < minLevel || minLevel == 0) {
					minLevel = currentLevel;
					return;
				}
			}

			if (pStructs[i].userId1 == userId1 || pStructs[i].userId2 == userId1)
			{
				currentLevelUsers[currentLevelUsersArrSize++] =
					pStructs[i].userId1 == userId1 ? pStructs[i].userId2 : pStructs[i].userId1; // sry for this
			}
			else
			{
				nextLevel[nextLevelArrSize++] = pStructs[i];
			}
		}

		if (currentLevel < minLevel || minLevel == 0) {
			for (int i = 0; i < currentLevelUsersArrSize; i++) {
				globalCurrentLevel = currentLevel;
				countLevel(nextLevel, nextLevelArrSize, currentLevelUsers[i], userId2);
			}
		}

		delete[] currentLevelUsers, nItems;
		delete[] nextLevel, nItems;
	}

	void countLevelWithPath(EndpointData* pStructs, int nItems, int userId1, int userId2)
	{
		if (pStructs == nullptr)
		{
			return;
		}

		EndpointData* nextLevel = new EndpointData[nItems];             // use this array to hold connections that don't contain userName1
		int* currentLevelUsers = new int[nItems];                   // use this array to hold users that are directly connected with userName1
		int nextLevelArrSize = 0;
		int currentLevelUsersArrSize = 0;
		int currentLevel = globalCurrentLevel + 1;
		ResultData res;
		res.userId = userId1;
		res.userNumberInPath = currentLevel;
		currentPath->push_back(res);
		for (int i = 0; i < nItems; i++)
		{
			if (pStructs[i].userId1 == userId1 && pStructs[i].userId2 == userId2 ||   // check if we've found a connection
				pStructs[i].userId2 == userId1 && pStructs[i].userId1 == userId2)     // between userName1 and userName2
			{
				if (currentLevel < minLevel || minLevel == 0) {
					minPath->clear();
					minPath = new std::vector<ResultData>(currentPath->size());
					std::copy(currentPath->begin(), currentPath->end(), minPath->begin());
					minLevel = currentLevel;
					currentPath->pop_back();
					return;
				}
			}

			if (pStructs[i].userId1 == userId1 || pStructs[i].userId2 == userId1)
			{
				currentLevelUsers[currentLevelUsersArrSize++] =
					pStructs[i].userId1 == userId1 ? pStructs[i].userId2 : pStructs[i].userId1; // sry for this
			}
			else
			{
				nextLevel[nextLevelArrSize++] = pStructs[i];
			}
		}

		if (currentLevel < minLevel || minLevel == 0) {
			for (int i = 0; i < currentLevelUsersArrSize; i++) {
				globalCurrentLevel = currentLevel;
				countLevelWithPath(nextLevel, nextLevelArrSize, currentLevelUsers[i], userId2);
			}
		}

		delete[] currentLevelUsers, nItems;
		delete[] nextLevel, nItems;
		currentPath->pop_back();
	}


	__declspec(dllexport) int __stdcall findPath(EndpointData* pStructs, int nItems, int userId1, int userId2)
	{
		minLevel = 0;
		globalCurrentLevel = 0;
		countLevel(pStructs, nItems, userId1, userId2);
		return minLevel;

	}

	__declspec(dllexport) ResultData* __stdcall findPathWithResultData(EndpointData* pStructs, int nItems, int userId1, int userId2, int& resultArraySize)
	{
		minLevel = 0;
		globalCurrentLevel = 0;
		minPath = new std::vector<ResultData>();
		currentPath = new std::vector<ResultData>();
		countLevelWithPath(pStructs, nItems, userId1, userId2);
		if (minPath->size() > 0)
		{
			ResultData destinationUser;
			destinationUser.userId = userId2;
			destinationUser.userNumberInPath = minPath->size();
			minPath->push_back(destinationUser);
		}
		resultArraySize = minPath->size();
		minPath->shrink_to_fit();
		return resultArraySize != 0 ? &minPath->at(0) : NULL;
	}


	int * __stdcall getFriend(EndpointData* friendShips, int length, int idUser, int & count)
	{
		count = 0;
		int * friends = new int[length];
		for (size_t i = 0; i < length; i++)
		{
			if (idUser == friendShips[i].userId1)
			{
				friends[count] = friendShips[i].userId2;
				++count;
			}
			if (idUser == friendShips[i].userId2)
			{
				friends[count] = friendShips[i].userId1;
				++count;
			}
		}
		return friends;
	}



	__declspec(dllexport) int * __stdcall getCommonFriends(EndpointData* friendShips, int length, int idUser1, int idUser2, int & lengthResult)
	{
		lengthResult = 0;

		int length1 = 0;
		int length2 = 0;

		int * friends1 = getFriend(friendShips, length, idUser1, length1);
		int * friends2 = getFriend(friendShips, length, idUser2, length2);

		int * commonFriends = new int[min(length1, length2)];
		for (size_t i = 0; i < length1; i++)
		{
			for (size_t j = 0; j < length2; j++)
			{
				if (friends1[i] == friends2[j])
				{
					commonFriends[lengthResult] = friends1[i];
					++lengthResult;
				}
			}
		}
		return commonFriends;
	}
}