﻿//using System;
//using DogeNetwork.Services.MathematicalCalculator;
//using NUnit.Framework;

//namespace UnitTests
//{
//    [TestFixture]
//    public class CalculatorTest
//    {
//        [Test]
//        public void Correct_Expression__Should_Return_Correct_Result__Example1()
//        {
//            string expression = "2*(-3.2+1.1)";
//            double result = -4.2;

//            MathematicalCalculator rpn = new MathematicalCalculator();
//            rpn.Parse(expression);

//            Assert.That(result.Equals(rpn.Evaluate()));
//        }

//        [Test]
//        public void Correct_Expression__Should_Return_Correct_Result__Example2()
//        {
//            string expression = "-4^-2";
//            double result = -0.0625;

//            MathematicalCalculator rpn = new MathematicalCalculator();
//            rpn.Parse(expression);

//            Assert.That(result.Equals(rpn.Evaluate()));
//        }

//        [Test]
//        public void Correct_Expression__Should_Return_Correct_Result__Example3()
//        {
//            string expression = "3+4*2/(-1-5)^2^3";
//            double result = 3.000004763;

//            MathematicalCalculator rpn = new MathematicalCalculator();
//            rpn.Parse(expression);

//            Assert.That(result.Equals(rpn.Evaluate()));
//        }

//        [Test]
//        public void When_There_Is_Not_RightParenthesis_After_LeftParenthesis__Parsing_Method_Should_Throw_Exception()
//        {
//            string expression = "2*(-4";

//            MathematicalCalculator rpn = new MathematicalCalculator();

//            Assert.That(() => rpn.Parse(expression),
//                Throws.TypeOf<Exception>().With.Message.EqualTo("Unbalanced parenthesis!"));
//        }

//        [Test]
//        public void When_There_Are_To_Few_Parameters__Evaluate_Method_Should_Throw_Exception()
//        {
//            string expression = "2*/4";

//            MathematicalCalculator rpn = new MathematicalCalculator();
//            rpn.Parse(expression);

//            Assert.That(() => rpn.Evaluate(),
//                Throws.TypeOf<Exception>().With.Message.EqualTo("Evaluation error!"));
//        }
//    }
//}
