﻿//using DogeNetwork.Contracts.Services;
//using DogeNetwork.Services;
//using DogeNetwork.Services.MathematicalCalculator;
//using DogeNetwork.Services.Web.TagCreators;
//using DogeNetwork.Services.Web.WhatIs;
//using NUnit.Framework;

//namespace UnitTests.ServiseTests
//{
//    [TestFixture]
//    public class MessageParserTests
//    {
//        [Test]
//        public void ParseUrl_ChangeOneUrlToTag_ReturnChangeMessage()
//        {
//            string message = "https://habrahabr.ru/";
//            string result = "<a href=\"https://habrahabr.ru/\">Интересные публикации / Хабрахабр</a>";
//            IMessageParser messageParser = new MessageParser(new MathematicalCalculator(), new WhatIsCommand(), new LinkTagCreator(), new EmailTagCreators());
//            Assert.That(result.Equals(messageParser.ParseUrl(message)));
//        }

//        [Test]
//        public void ParseUrl_ChangeOneUrlToTag_ReturnMessageWithError()
//        {
//            string message = "https://habrr.ru/";
//            string result = "<a href=\"https://habrr.ru/\">https://habrr.ru/</a>";
//            IMessageParser messageParser = new MessageParser(new MathematicalCalculator(), new WhatIsCommand(), new LinkTagCreator(), new EmailTagCreators());
//            Assert.That(result.Equals(messageParser.ParseUrl(message)));
//        }

//        [Test]
//        public void ParseUrl_ChangeTwoUrlToTag_ReturnChangeMessage()
//        {
//            string message = "https://habrahabr.ru/ dsfsdwersdf http://stackoverflow.com/";
//            string result = "<a href=\"https://habrahabr.ru/\">Интересные публикации / Хабрахабр</a> dsfsdwersdf <a href=\"http://stackoverflow.com/\">Stack Overflow</a>";
//            IMessageParser messageParser = new MessageParser(new MathematicalCalculator(), new WhatIsCommand(), new LinkTagCreator(), new EmailTagCreators()); Assert.That(result.Equals(messageParser.ParseUrl(message)));
//        }

//        [Test]
//        public void ParseEmail_ChangeOneEmaiToTag_ReturnChangeMessage()
//        {
//            string message = "ma@hostname.com";
//            string result = "<a href=\"mailto:ma@hostname.com\">ma@hostname.com</a>";
//            IMessageParser messageParser = new MessageParser(new MathematicalCalculator(), new WhatIsCommand(), new LinkTagCreator(), new EmailTagCreators()); Assert.That(result.Equals(messageParser.ParseEmail(message)));
//        }

//        [Test]
//        public void ParseEmail_ChangeManyEmaiToTag_ReturnChangeMessage()
//        {
//            string message = "sdfsdf ma@hostname.com ma-a@hostname.com.edu fvfasvfhysd 12@hostname.com";
//            string result = "sdfsdf <a href=\"mailto:ma@hostname.com\">ma@hostname.com</a> <a href=\"mailto:ma-a@hostname.com.edu\">ma-a@hostname.com.edu</a> fvfasvfhysd <a href=\"mailto:12@hostname.com\">12@hostname.com</a>";
//            IMessageParser messageParser = new MessageParser(new MathematicalCalculator(), new WhatIsCommand(), new LinkTagCreator(), new EmailTagCreators()); Assert.That(result.Equals(messageParser.ParseEmail(message)));
//        }

//        [Test]
//        public void ParseMath_CalcNumber_ReturnResult()
//        {
//            string message = "72 + (26 - 12) + 8";
//            string result = "94";
//            IMessageParser messageParser = new MessageParser(new MathematicalCalculator(), new WhatIsCommand(), new LinkTagCreator(), new EmailTagCreators()); Assert.That(result.Equals(messageParser.ParseMath(message)));
//        }

//        [Test]
//        public void ParseMath_CalcNumber_ReturnErrorMessage()
//        {
//            string message = "(1 + 1( some text";
//            string result = "(1 + 1(Cannot calculate)( some text";
//            IMessageParser messageParser = new MessageParser(new MathematicalCalculator(), new WhatIsCommand(), new LinkTagCreator(), new EmailTagCreators()); Assert.That(result.Equals(messageParser.ParseMath(message)));
//        }

//        [Test]
//        public void ParseWahtIs_FindWord_ReturnResult()
//        {
//            string message = "what is Wiki";
//            string result = "Wiki is...\n A wiki (i/ˈwɪki/ WIK-ee) is a website which allows collaborative modification of its content and structure directly from the web browser. In a typical wiki, text is written using a simplified markup language (known as \"wiki markup\"), and often edited with the help of a rich-text editor.[1] \n";
//            IMessageParser messageParser = new MessageParser(new MathematicalCalculator(), new WhatIsCommand(), new LinkTagCreator(), new EmailTagCreators()); Assert.That(result.Equals(messageParser.ParseWhatIs(message)));
//        }

//        [Test]
//        public void ParseWahtIs_FindTwoWord_ReturnResult()
//        {
//            string message = "what is Vagabond (magazine)";
//            string result = "Vagabond (magazine) is...\n Vagabond is a travel magazine published in Stockholm, Sweden. The magazine is one of the earliest travel magazines in the country and publishes travel-related articles. \n";
//            IMessageParser messageParser = new MessageParser(new MathematicalCalculator(), new WhatIsCommand(), new LinkTagCreator(), new EmailTagCreators()); Assert.That(result.Equals(messageParser.ParseWhatIs(message)));
//        }

//        [Test]
//        public void ParseWahtIs_FindWord_ReturnFail()
//        {
//            string message = "what is Tfhd";
//            string result = "Tfhd is...\n Try enter another data \n";
//            IMessageParser messageParser = new MessageParser(new MathematicalCalculator(), new WhatIsCommand(), new LinkTagCreator(), new EmailTagCreators()); Assert.That(result.Equals(messageParser.ParseWhatIs(message)));
//        }

//    }
//}