﻿using DogeWebService.Contracts.Models;
using DogeWebService.Contracts.Models.Infrastructure;
using DogeWebService.Contracts.Models.Services;
using DogeWebService.Contracts.Presenters;
using DogeWebService.Contracts.Services;
using DogeWebService.Contracts.Services.MathematicalCalculator;
using DogeWebService.Contracts.Services.Web;
using DogeWebService.Models;
using DogeWebService.Models.Infrastructure;
using DogeWebService.Models.Services;
using DogeWebService.Presenters;
using DogeWebService.Services;
using DogeWebService.Services.MathematicalCalculator;
using DogeWebService.Services.Web.HTMLParser;
using DogeWebService.Services.Web.TagCreators;
using DogeWebService.Services.Web.WhatIs;
using Microsoft.Practices.Unity;

namespace DogeWebService
{
    public class ServiceLocator
    {
        private readonly IUnityContainer _container;

        private static ServiceLocator _instance;
        public static ServiceLocator Instance
        {
            get
            {
                return _instance ?? (_instance = new ServiceLocator());
            }
        }

        private ServiceLocator()
        {
            _container = new UnityContainer();

            InitializePresenters(_container);
            InitializeModels(_container);
            InitiaizeServices(_container);
        }

        public T Resolve<T>()
        {
            return _container.Resolve<T>();
        }

        private void InitializePresenters(IUnityContainer container)
        {
            container.RegisterType<IUserHandler, UserHandler>();
            container.RegisterType<IBot, Bot>();
        }

        private void InitializeModels(IUnityContainer container)
        {
            InjectionConstructor injectionConstructor = new InjectionConstructor("conStr");
            container.RegisterType<IConnectionFactory, DbConnectionFactory>(injectionConstructor);

            container.RegisterType<IBotDal, BotDal>();
            container.RegisterType<IChatDal, ChatDal>();
            container.RegisterType<IUserFriendshipDal, UserFriendshipDal>();
            container.RegisterType<IUserHandlerDal, UserHandlerDal>();
            container.RegisterType<IWhatIsDataLayer, WhatIsDataLayer>();
            container.RegisterType<IUserCheckerDal, UserCheckerDal>();
        }

        private void InitiaizeServices(IUnityContainer container)
        {
            container.RegisterType<IMessageParser, MessageParser>();
            container.RegisterType<IMathematicalCalculator, MathematicalCalculator>();
            container.RegisterType<IHtmlParser, HtmlTitleParser>();
            container.RegisterType<ITagCreator, LinkTagCreator>("LinkTagCreator", new ContainerControlledLifetimeManager());
            container.RegisterType<ITagCreator, EmailTagCreators>("EmailTagCreator", new ContainerControlledLifetimeManager());
            container.RegisterType<IWhatIsCommand, WhatIsCommand>();
            container.RegisterType<IUserChecker, UserChecker>();
        }
    }
}
