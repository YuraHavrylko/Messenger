﻿using System;
using System.Runtime.InteropServices;
using DataContracts.Entities;

namespace DogeWebService
{
    class NativeMethods
    {
        [DllImport(@"..\..\..\x64\Debug\UsersConnections.dll",
        EntryPoint = "findPath")]
        public static extern int findPath(Connection[] pStructs, int nItems, int userName1, int userName2);

        [DllImport(@"..\..\..\x64\Debug\UsersConnections.dll",
            EntryPoint = "findPathWithResultData")]
        public static extern IntPtr findPathWithResultData(Connection[] pStructs,
            int nItems, int userName1, int userName2, out int resultArraySize);

        [DllImport(@"..\..\..\x64\Debug\UsersConnections.dll",
            EntryPoint = "getCommonFriends", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi)]
        public static extern IntPtr getCommonFriends(Connection[] friendShips, int length, int nameUser1,
           int nameUser2, out int lengthResult);
    }
}
