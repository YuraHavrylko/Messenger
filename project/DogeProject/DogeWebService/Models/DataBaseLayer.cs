﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using DataContracts.Entities;
using DogeWebService.Contracts.Models;
using DogeWebService.Contracts.Models.Infrastructure;
using DogeWebService.Models.Infrastructure;
using DogeWebService.Services;

namespace DogeWebService.Models
{
    public class DataBaseLayer : Repository, IDataBaseLayer
    {
        private readonly string _connectionString = AppSettings.Instance.ConnectionString;

        public DataBaseLayer(IConnectionFactory connectionFactory)
            : base(connectionFactory)
        {
        }

        public List<User> GetUsers()
        {
            return CustomExecuteReader<User>("sp_GetUsers").ToList();
        }

        public User GetUser(string userName)
        {
            Dictionary<string, object> parameters = new Dictionary<string, object> { { "@currentUserName", userName } };

            return CustomExecuteReader<User>("sp_GetUser", parameters).FirstOrDefault();
        }

        public void AddUserToDb(string userName, string password)
        {
            Dictionary<string, object> parameters = new Dictionary<string, object>
            {
                {"@username", userName},
                {"@password", password}
            };

            CustomExecuteNonQuery("sp_InsertUser", parameters);
        }

        public void DeleteUserFromDb(string userName)
        {
            Dictionary<string, object> parameters = new Dictionary<string, object> { { "@username", userName } };

            CustomExecuteNonQuery("sp_RemoveUser", parameters);
        }



        public void AddMessageToDb(int userFrom, int userTo, string originalMessage, string modifiedMessage)
        {
            Dictionary<string, object> parameters = new Dictionary<string, object>
            {
                {"@userFrom", userFrom},
                {"@userTo", userTo},
                {"@originalMessage", originalMessage},
                {"@ModifiedMessage", modifiedMessage}
            };

            CustomExecuteNonQuery("sp_InsertMessage", parameters);
        }

        public void DeleteUsersMessages(int currentUser)
        {
            Dictionary<string, object> parameters = new Dictionary<string, object> { { "@currentUser", currentUser } };

            CustomExecuteNonQuery("sp_DeleteMessages", parameters);
        }

        public List<Message> GetDialog(int currentUserId, int secondUserId)
        {
            Dictionary<string, object> parameters = new Dictionary<string, object>
            {
                { "@CurrentUserId", currentUserId },
                { "@SecondUserId", secondUserId }
            };

            return CustomExecuteReader<Message>("GetDialog", parameters).ToList();
        }
        

        public List<User> GetUsersFriends(int currentUser)
        {
            Dictionary<string, object> parameters = new Dictionary<string, object> {{"@currentUser", currentUser}};

            return CustomExecuteReader<User>("sp_GetUsersFriends", parameters).ToList();
        }

        public List<Connection> GetConnections()
        {
            return CustomExecuteReader<Connection>("sp_GetConnections").ToList();
        }

        public bool CheckUserExistence(string currentUser)
        {
            Dictionary<string, object> parameters = new Dictionary<string, object> {{"@currentUserName", currentUser}};

            return CustomExecuteReader<User>("CheckUserExistence", parameters).ToList().Count != 0;
        }

        public bool CheckUserData(string userName, string password)
        {
            Dictionary<string, object> parameters = new Dictionary<string, object>
            {
                {"@currentUserName", userName},
                {"@currentUserPassword", password}
            };

            return CustomExecuteReader<User>("sp_CheckUserData", parameters).ToList().Count != 0;
        }



        public int GetNumberOfPhrases()
        {
            return Convert.ToInt32(CustomExecuteScalar("GetnumberOfIdPhrases"));
        }

        public BotPhrase GetPhraseById(int id)
        {
            Dictionary<string, object> parameters = new Dictionary<string, object> {{"@currentId", id}};

            return CustomExecuteReader<BotPhrase>("GetPhraseById", parameters).FirstOrDefault();
        }

        public string GetCookie(string userName)
        {
            Dictionary<string, object> parameters = new Dictionary<string, object> { { "@currentUserName", userName } };

            var user = CustomExecuteReader<User>("sp_GetUser", parameters).FirstOrDefault();
            return user.cookie;
        }

        public void SetCookie(string userName, string cookie)
        {
            Dictionary<string, object> parameters = new Dictionary<string, object> { { "@currentNameUser", userName }, { "@currentCookie", cookie} };
            CustomExecuteNonQuery("sp_SetCookie", parameters);
        }
    }
}