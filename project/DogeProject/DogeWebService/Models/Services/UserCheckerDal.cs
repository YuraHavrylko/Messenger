﻿using System.Collections.Generic;
using System.Linq;
using DataContracts.Entities;
using DogeWebService.Contracts.Models.Infrastructure;
using DogeWebService.Contracts.Models.Services;
using DogeWebService.Models.Infrastructure;

namespace DogeWebService.Models.Services
{
    public class UserCheckerDal : Repository, IUserCheckerDal
    {
        public UserCheckerDal(IConnectionFactory connectionFactory)
            : base(connectionFactory)
        {
        }


        public bool CheckUserExistence(string currentUser)
        {
            Dictionary<string, object> parameters = new Dictionary<string, object> { { "@currentUserName", currentUser } };

            return CustomExecuteReader<User>("CheckUserExistence", parameters).ToList().Count != 0;
        }

        public bool CheckUserExistenceById(int id)
        {
            Dictionary<string, object> parameters = new Dictionary<string, object> { { "@userId", id } };

            return CustomExecuteReader<User>("CheckUserExistenceById", parameters).ToList().Count != 0;
        }
    }
}
