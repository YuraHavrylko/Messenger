﻿using System.Collections.Generic;
using System.Linq;
using DataContracts.Entities;
using DogeWebService.Contracts.Models.Infrastructure;
using DogeWebService.Contracts.Models.Services;
using DogeWebService.Models.Infrastructure;

namespace DogeWebService.Models.Services
{
    public class WhatIsDataLayer : Repository, IWhatIsDataLayer
    {
        public WhatIsDataLayer(IConnectionFactory connectionFactory)
            : base(connectionFactory)
        {
        }

        public void AddNewArticle(string articleName, string articleParagraph)
        {
            Dictionary<string, object> parameters = new Dictionary<string, object>
            {
                {"@ArticleName", articleName},
                {"@ArticleParagraph", articleParagraph}
            };

            CustomExecuteNonQuery("WhatIsAdd", parameters);
        }

        public WhatIsArticle GetArticle(string articleName)
        {
            Dictionary<string, object> parameters = new Dictionary<string, object> { { "@ArticleName", articleName } };

            return CustomExecuteReader<WhatIsArticle>("WhatIsGet", parameters).FirstOrDefault();
        }
    }
}
