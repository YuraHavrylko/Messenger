﻿using System.Collections.Generic;
using System.Linq;
using DataContracts.Entities;
using DogeWebService.Contracts.Models;
using DogeWebService.Contracts.Models.Infrastructure;
using DogeWebService.Models.Infrastructure;

namespace DogeWebService.Models
{
    public class UserFriendshipDal : Repository, IUserFriendshipDal
    {
        public UserFriendshipDal(IConnectionFactory connectionFactory)
            : base(connectionFactory)
        {
        }
        
        public List<UserMetadata> GetUsersFriends(int currentUser)
        {
            Dictionary<string, object> parameters = new Dictionary<string, object> {{"@currentUser", currentUser}};

            return CustomExecuteReader<UserMetadata>("sp_GetUsersFriends", parameters).ToList();
        }

        public List<Connection> GetConnections()
        {
            return CustomExecuteReader<Connection>("sp_GetConnections").ToList();
        }

        public UserMetadata GetUserById(int id)
        {
            Dictionary<string, object> parameters = new Dictionary<string, object> { { "@userId", id } };

            return CustomExecuteReader<UserMetadata>("sp_GetUserById", parameters).FirstOrDefault();
        }
    }
}