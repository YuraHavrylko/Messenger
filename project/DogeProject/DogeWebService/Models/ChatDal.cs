﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataContracts.Entities;
using DogeWebService.Contracts.Models;
using DogeWebService.Contracts.Models.Infrastructure;
using DogeWebService.Models.Infrastructure;

namespace DogeWebService.Models
{
    public class ChatDal : Repository, IChatDal
    {
        public ChatDal(IConnectionFactory connectionFactory)
            : base(connectionFactory)
        {
        }

        public void AddMessageToDb(int userFromId, int userToId, string originalMessage, string modifiedMessage, DateTime timeOfSending)
        {
            Dictionary<string, object> parameters = new Dictionary<string, object>
            {
                {"@fromUser", userFromId},
                {"@toUser", userToId},
                {"@originalMessage", originalMessage},
                {"@ModifiedMessage", modifiedMessage},
                {"@TimeOfSending", timeOfSending}
            };

            CustomExecuteNonQuery("sp_InsertMessage", parameters);
        }

        public void DeleteUsersMessages(int userId)
        {
            Dictionary<string, object> parameters = new Dictionary<string, object> { { "@currentUser", userId } };

            CustomExecuteNonQuery("sp_DeleteMessages", parameters);
        }

        public List<Message> GetDialog(int userFromId, int userToId)
        {
            Dictionary<string, object> parameters = new Dictionary<string, object>
            {
                { "@CurrentUserId", userFromId },
                { "@SecondUserId", userToId }
            };

            return CustomExecuteReader<Message>("GetDialog", parameters).ToList();
        }

        public List<Message> GetDialogFromDate(int userFromId, int userToId, DateTime fromDate)
        {
            Dictionary<string, object> parameters = new Dictionary<string, object>
            {
                { "@CurrentUserId", userFromId },
                { "@SecondUserId", userToId },
                { "@FromDate", fromDate }
            };

            return CustomExecuteReader<Message>("GetDialogFromDate", parameters).ToList();
        }

        public List<Message> GetAllUserDialogs(int id)
        {
            Dictionary<string, object> parameters = new Dictionary<string, object> { { "@userId", id } };

            return CustomExecuteReader<Message>("sp_GetUserDialogs", parameters).ToList();
        }
    }
}