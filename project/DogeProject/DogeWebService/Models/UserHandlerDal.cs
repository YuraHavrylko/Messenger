﻿using System.Collections.Generic;
using System.Linq;
using DataContracts.Entities;
using DogeWebService.Contracts.Models;
using DogeWebService.Contracts.Models.Infrastructure;
using DogeWebService.Models.Infrastructure;

namespace DogeWebService.Models
{
    public class UserHandlerDal : Repository, IUserHandlerDal
    {
        public UserHandlerDal(IConnectionFactory connectionFactory)
            : base(connectionFactory)
        {
        }

        public List<User> GetUsers()
        {
            return CustomExecuteReader<User>("sp_GetUsers").ToList();
        }

        public User GetLoginAndPass(int id)
        {
            Dictionary<string, object> parameters = new Dictionary<string, object> { { "@userId", id } };

            return CustomExecuteReader<User>("sp_GetUserById", parameters).FirstOrDefault();
        }

        public UserMetadata GetUserById(int id)
        {
            Dictionary<string, object> parameters = new Dictionary<string, object> { { "@userId", id } };

            return CustomExecuteReader<UserMetadata>("sp_GetUserById", parameters).FirstOrDefault();
        }

        public UserMetadata GetUserByUserName(string userName)
        {
            Dictionary<string, object> parameters = new Dictionary<string, object> {{"@currentUserName", userName}};

            return CustomExecuteReader<UserMetadata>("sp_GetUser", parameters).FirstOrDefault();
        }

        public List<UserMetadata> SearchUser(string userName)
        {
            Dictionary<string, object> parameters = new Dictionary<string, object> { { "@predicate", userName } };

            return CustomExecuteReader<UserMetadata>("sp_SearchUser", parameters).ToList();
        }

        public void AddUserToDb(string userName, string password)
        {
            Dictionary<string, object> parameters = new Dictionary<string, object>
            {
                {"@username", userName},
                {"@password", password}
            };

            CustomExecuteNonQuery("sp_InsertUser", parameters);
        }

        public int DeleteUserFromDb(int id)
        {
            Dictionary<string, object> parameters = new Dictionary<string, object> {{ "@userId", id } };

            return CustomExecuteNonQuery("sp_RemoveUser", parameters);
        }

        public bool CheckUserData(string userName, string password)
        {
            Dictionary<string, object> parameters = new Dictionary<string, object>
            {
                {"@currentUserName", userName},
                {"@currentUserPassword", password}
            };

            return CustomExecuteReader<User>("sp_CheckUserData", parameters).ToList().Count != 0;
        }
    }
}