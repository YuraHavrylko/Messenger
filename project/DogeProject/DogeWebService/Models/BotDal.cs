﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataContracts.Entities;
using DogeWebService.Contracts.Models;
using DogeWebService.Contracts.Models.Infrastructure;
using DogeWebService.Models.Infrastructure;

namespace DogeWebService.Models
{
    public class BotDal : Repository, IBotDal
    {
        public BotDal(IConnectionFactory connectionFactory)
            : base(connectionFactory)
        {
        }

        public int GetNumberOfPhrases()
        {
            return Convert.ToInt32(CustomExecuteScalar("GetnumberOfIdPhrases"));
        }

        public BotPhrase GetPhraseById(int id)
        {
            Dictionary<string, object> parameters = new Dictionary<string, object> {{"@currentId", id}};

            return CustomExecuteReader<BotPhrase>("GetPhraseById", parameters).FirstOrDefault();
        }
    }
}