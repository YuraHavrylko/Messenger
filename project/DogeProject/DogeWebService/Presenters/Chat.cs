﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using DataContracts.Entities;
using DogeWebService.Contracts.Models;
using DogeWebService.Contracts.Presenters;
using DogeWebService.Contracts.Services;
using DogeWebService.Models;
using DogeWebService.Services;

namespace DogeWebService.Presenters
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall
        , ConcurrencyMode = ConcurrencyMode.Multiple)]
    public class Chat : IChat
    {
        private readonly IChatDal _dataLayer;
        private readonly IMessageParser _parser;
        private readonly IUserChecker _userChecker;

        public Chat()//(IDataBaseLayer dataLayer, IMessageParser parser)
        {
            _dataLayer = ServiceLocator.Instance.Resolve<ChatDal>();
            _parser = ServiceLocator.Instance.Resolve<MessageParser>();
            _userChecker = ServiceLocator.Instance.Resolve<UserChecker>();
        }


        public void WriteMessage(int userId1, int userId2, string message)
        {
            var modifiedMessage = MessageHandler(message);

            if (_userChecker.CheckUserExistenceById(userId1) && _userChecker.CheckUserExistenceById(userId2))
            {
                _dataLayer.AddMessageToDb(userId1, userId2, message, modifiedMessage, DateTime.Now);
            }

            _userChecker.CheckBothUsersExistenceById(userId1, userId2);
        }

        public List<Message> GetDialog(int userId1, int userId2)
        {
            return _dataLayer.GetDialog(userId1, userId2);
        }

        public List<Message> GetDialogFromDate(int userId1, int userId2, int fromDate)
        {
            return _dataLayer.GetDialogFromDate(userId1, userId2, new DateTime(1970, 1, 1).AddSeconds(fromDate));
        }

        public List<Message> GetAllUserDialogs(int userId)
        {
            return _dataLayer.GetAllUserDialogs(userId);
        }


        private string MessageHandler(string message)
        {
            message = _parser.ParseEmail(message);
            message = _parser.ParseUrl(message);
            message = _parser.ParseMath(message);
            return message;
        }
    }
}
