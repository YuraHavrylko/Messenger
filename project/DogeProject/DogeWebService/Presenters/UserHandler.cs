﻿using System.Collections.Generic;
using System.IO;
using System.ServiceModel;
using DataContracts.Entities;
using DogeWebService.Contracts.Models;
using DogeWebService.Contracts.Presenters;
using DogeWebService.Contracts.Services;
using DogeWebService.Models;
using DogeWebService.Services;

namespace DogeWebService.Presenters
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall
        , ConcurrencyMode = ConcurrencyMode.Multiple)]
    public class UserHandler : IUserHandler
    {
        private readonly IUserHandlerDal _dataLayer;
        private readonly IUserChecker _userChecker;

        public UserHandler()//(IDataBaseLayer dataLayer, IMessageParser parser)
        {
            _dataLayer = ServiceLocator.Instance.Resolve<UserHandlerDal>();
            _userChecker = ServiceLocator.Instance.Resolve<UserChecker>();
        }

        public List<User> GetUsers()
        {
            return _dataLayer.GetUsers();
        }

        public UserMetadata GetUserById(int id)
        {
            return _dataLayer.GetUserById(id);
        }

        public UserMetadata GetUserByUserName(string currentUser)
        {
            return _dataLayer.GetUserByUserName(currentUser);
        }

        public List<UserMetadata> SearchUser(string userName)
        {
            return _dataLayer.SearchUser(userName);
        }

        public void AddUser(string userName, string password)
        {
            if (!_userChecker.CheckUserExistence(userName))
            {
                _dataLayer.AddUserToDb(userName, password);
            }
            else
            {
                throw new InvalidDataException($"User with username: \"{userName}\" exists, choose another username");
            }
        }

        public void DeleteUser(int userId)
        {
            if (_dataLayer.DeleteUserFromDb(userId) == 0)
            {
                throw new InvalidDataException($"This user doesn't exists");
            }
        }

        public bool Login(string userName, string password)
        {
            return _dataLayer.CheckUserData(userName, password);
        }

        public bool CheckUserExistence(string userName)
        {
            return _userChecker.CheckUserExistence(userName);
        }

        public bool CheckUserExistenceById(int id)
        {
            return _userChecker.CheckUserExistenceById(id);
        }

        public void CheckBothUsersExistenceByName(string userName1, string userName2)
        {
            _userChecker.CheckBothUsersExistenceByName(userName1, userName2);
        }
    }
}
