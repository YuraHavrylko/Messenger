﻿using System;
using System.ServiceModel;
using DogeWebService.Contracts.Models;
using DogeWebService.Contracts.Presenters;
using DogeWebService.Contracts.Services;
using DogeWebService.Models;
using DogeWebService.Services;

namespace DogeWebService.Presenters
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall
            ,ConcurrencyMode = ConcurrencyMode.Multiple)]
    public class Bot: IBot
    {
        private readonly IBotDal _dataLayer;
        private readonly IMessageParser _parser;
        private Random randomNumber = new Random();

        public Bot()//(IDataBaseLayer dataLayer, IMessageParser parser)
        {
            _dataLayer = ServiceLocator.Instance.Resolve<BotDal>();
            _parser = ServiceLocator.Instance.Resolve<MessageParser>();
        }

        public string MessageHandler(string message)
        {
            var oldMessage = message;
            if (message == string.Empty)
                return "Write me something";
            else
            {
                var tempMessage = _parser.ParseWhatIs(message);
                if (tempMessage == string.Empty)
                {
                    message = _parser.ParseEmail(message);
                    message = _parser.ParseUrl(message);
                    message = _parser.ParseMath(message);

                    if (message != oldMessage)
                        return message;
                    else
                    {
                        int idPhrases = _dataLayer.GetNumberOfPhrases();
                        if (idPhrases == 0)
                            return "Hmmm.....";
                        else
                        {
                            return _dataLayer.GetPhraseById(randomNumber.Next(1, idPhrases)).phrase;
                        }
                       
                    }
                }
                else
                {
                    return tempMessage;
                }
            }
        }
        
    }
}
