﻿using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.ServiceModel;
using DataContracts.Entities;
using DogeWebService.Contracts.Models;
using DogeWebService.Contracts.Presenters;
using DogeWebService.Models;

namespace DogeWebService.Presenters
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall
            , ConcurrencyMode = ConcurrencyMode.Multiple)]
    public class UserFriendship : IUserFriendship
    {
        private readonly IUserFriendshipDal _dataLayer;

        public UserFriendship()
        {
            _dataLayer = ServiceLocator.Instance.Resolve<UserFriendshipDal>();
        }

        public List<UserMetadata> GetUsersFriends(int userId)
        {
            return _dataLayer.GetUsersFriends(userId);
        }

        public int FindPath(int userId1, int userId2)
        {
            List<Connection> friendship = _dataLayer.GetConnections();

            return NativeMethods.findPath(friendship.ToArray(), friendship.ToArray().Length, userId1, userId2);
        }

        public List<UserMetadata> FindCommonFriends(int userId1, int userId2)
        {
            List<Connection> friendship = _dataLayer.GetConnections();
            int resultArraySize;
            var pointer = NativeMethods.getCommonFriends(friendship.ToArray(),
                            friendship.ToArray().Length,
                            userId1,
                            userId2,
                            out resultArraySize);

            int[] arrayOfUsers = new int[resultArraySize];

            List<UserMetadata> commonFriends = new List<UserMetadata>();

            for (int i = 0; i < resultArraySize; i++)
            {
                arrayOfUsers[i] = Marshal.PtrToStructure<int>(pointer);
                pointer = pointer + Marshal.SizeOf<int>();

                commonFriends.Add(_dataLayer.GetUserById(arrayOfUsers[i]));
            }

            return commonFriends;
        }


        public List<UserMetadata> FindPathWithResultData(int userId1, int userId2)
        {
            List<Connection> friendship = _dataLayer.GetConnections();
            int resultArraySize;

            var pointer = NativeMethods.findPathWithResultData(friendship.ToArray(),
                            friendship.ToArray().Length,
                            userId1, userId2,
                            out resultArraySize);

            ResultData[] arrayOfUsers = new ResultData[resultArraySize];
            List<UserMetadata> commonFriends = new List<UserMetadata>();

            for (int i = 0; i < resultArraySize; i++)
            {
                arrayOfUsers[i] = Marshal.PtrToStructure<ResultData>(pointer);
                pointer = pointer + Marshal.SizeOf<ResultData>();
                commonFriends.Add(_dataLayer.GetUserById(arrayOfUsers[i].userId));
            }

            return commonFriends;
        }
    }
}