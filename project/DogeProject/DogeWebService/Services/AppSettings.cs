﻿using System.Configuration;

namespace DogeWebService.Services
{
    class AppSettings
    {
        private static AppSettings _instance;
        public static AppSettings Instance
        {
            get
            {
                return _instance ?? (_instance = new AppSettings());
            }
        }

        private AppSettings()
        {
        }

        public string UsersFilePath
        {
            get
            {
                return ConfigurationManager.AppSettings["usersFilePath"];
            }
        }

        public string UsersConnectionsFilePath
        {
            get
            {
                return ConfigurationManager.AppSettings["usersConnectionsFilePath"];
            }
        }

        public string MessagesFilePath
        {
            get { return ConfigurationManager.AppSettings["messagesFilePath"]; }
        }

        public string WhatIsFilePath
        {
            get
            {
                return ConfigurationManager.AppSettings["whatIsFilePath"];
            }
        }
        public string PhrasesFilePath
        {
            get
            {
                return ConfigurationManager.AppSettings["phrasesFilePath"];
            }
        }
        public string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["conStr"].ConnectionString;

            }
        }

    }
}
