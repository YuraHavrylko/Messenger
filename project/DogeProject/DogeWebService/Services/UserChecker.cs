﻿using System.IO;
using DogeWebService.Contracts.Models.Services;
using DogeWebService.Contracts.Services;

namespace DogeWebService.Services
{
    public class UserChecker : IUserChecker
    {
        private readonly IUserCheckerDal _dataLayer;

        public UserChecker()//(IDataBaseLayer dataLayer, IMessageParser parser)
        {
            _dataLayer = ServiceLocator.Instance.Resolve<IUserCheckerDal>();
        }


        public bool CheckUserExistence(string userName)
        {
            return _dataLayer.CheckUserExistence(userName);
        }

        public bool CheckUserExistenceById(int id)
        {
            return _dataLayer.CheckUserExistenceById(id);
        }

        public void CheckBothUsersExistenceByName(string userName1, string userName2)
        {
            bool user1Exists = CheckUserExistence(userName1);
            bool user2Exists = CheckUserExistence(userName2);

            if (!user1Exists && !user2Exists)
            {
                throw new InvalidDataException("None of these users exist");
            }
            if (!user1Exists)
            {
                throw new InvalidDataException($"User \"{userName1}\" doesn't exist");
            }
            if (!user2Exists)
            {
                throw new InvalidDataException($"User \"{userName2}\" doesn't exist");
            }
        }

        public void CheckBothUsersExistenceById(int userId1, int userId2)
        {
            bool user1Exists = CheckUserExistenceById(userId1);
            bool user2Exists = CheckUserExistenceById(userId2);

            if (!user1Exists || !user2Exists)
            {
                throw new InvalidDataException("At list one of these users don't exist");
            }
        }
    }
}
