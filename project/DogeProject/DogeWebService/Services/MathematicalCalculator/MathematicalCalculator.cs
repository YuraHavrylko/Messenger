﻿using System;
using System.Collections;
using System.Text.RegularExpressions;
using DogeWebService.Contracts.Services.MathematicalCalculator;
using DogeWebService.Services.MathematicalCalculator.Entities;

namespace DogeWebService.Services.MathematicalCalculator
{
    public class MathematicalCalculator : IMathematicalCalculator
    {
        private Queue _output;
        private Stack _ops;

        private string _sOriginalExpression;
        public string OriginalExpression
        {
            get { return _sOriginalExpression; }
        }

        private string _sTransitionExpression;
        public string TransitionExpression
        {
            get { return _sTransitionExpression; }
        }

        private string _sPostfixExpression;
        public string PostfixExpression
        {
            get { return _sPostfixExpression; }
        }

        public MathematicalCalculator()
        {
            _sOriginalExpression = string.Empty;
            _sTransitionExpression = string.Empty;
            _sPostfixExpression = string.Empty;
        }

        public void Parse(string expression)
        {
            _output = new Queue();
            _ops = new Stack();

            _sOriginalExpression = expression;

            string sBuffer = expression.ToLower();
            sBuffer = Regex.Replace(sBuffer, @"(?<number>\d+(\.\d+)?)", " ${number} ");
            sBuffer = Regex.Replace(sBuffer, @"(?<ops>[+\-*/^()])", " ${ops} ");
            sBuffer = Regex.Replace(sBuffer, "(?<alpha>(pi|e|sin|cos|tan))", " ${alpha} ");
            sBuffer = sBuffer.Replace('.', ',');
            sBuffer = Regex.Replace(sBuffer, @"\s+", " ").Trim();

            sBuffer = Regex.Replace(sBuffer, "-", "MINUS");
            sBuffer = Regex.Replace(sBuffer, @"(?<number>(pi|e|(\d+(\.\d+)?)))\s+MINUS", "${number} -");
            sBuffer = Regex.Replace(sBuffer, "MINUS", "~");

            _sTransitionExpression = sBuffer;

            string[] saParsed = sBuffer.Split(" ".ToCharArray());
            int i;
            double tokenvalue;
            ReversePolishNotationToken token, opstoken;
            for (i = 0; i < saParsed.Length; ++i)
            {
                token = new ReversePolishNotationToken();
                token.TokenValue = saParsed[i];
                token.TokenValueType = TokenType.None;

                try
                {
                    tokenvalue = double.Parse(saParsed[i]);
                    token.TokenValueType = TokenType.Number;
                    _output.Enqueue(token);
                }
                catch
                {
                    switch (saParsed[i])
                    {
                        case "+":
                            token.TokenValueType = TokenType.Plus;
                            if (_ops.Count > 0)
                            {
                                opstoken = (ReversePolishNotationToken)_ops.Peek();
                                while (IsOperatorToken(opstoken.TokenValueType))
                                {
                                    _output.Enqueue(_ops.Pop());
                                    if (_ops.Count > 0)
                                    {
                                        opstoken = (ReversePolishNotationToken)_ops.Peek();
                                    }
                                    else
                                    {
                                        break;
                                    }
                                }
                            }
                            _ops.Push(token);
                            break;
                        case "-":
                            token.TokenValueType = TokenType.Minus;
                            if (_ops.Count > 0)
                            {
                                opstoken = (ReversePolishNotationToken)_ops.Peek();
                                while (IsOperatorToken(opstoken.TokenValueType))
                                {
                                    _output.Enqueue(_ops.Pop());
                                    if (_ops.Count > 0)
                                    {
                                        opstoken = (ReversePolishNotationToken)_ops.Peek();
                                    }
                                    else
                                    {
                                        break;
                                    }
                                }
                            }
                            _ops.Push(token);
                            break;
                        case "*":
                            token.TokenValueType = TokenType.Multiply;
                            if (_ops.Count > 0)
                            {
                                opstoken = (ReversePolishNotationToken)_ops.Peek();

                                while (IsOperatorToken(opstoken.TokenValueType))
                                {
                                    if (opstoken.TokenValueType == TokenType.Plus || opstoken.TokenValueType == TokenType.Minus)
                                    {
                                        break;
                                    }
                                    else
                                    {
                                        _output.Enqueue(_ops.Pop());
                                        if (_ops.Count > 0)
                                        {
                                            opstoken = (ReversePolishNotationToken)_ops.Peek();
                                        }
                                        else
                                        {
                                            break;
                                        }
                                    }
                                }
                            }

                            _ops.Push(token);
                            break;
                        case "/":
                            token.TokenValueType = TokenType.Divide;
                            if (_ops.Count > 0)
                            {
                                opstoken = (ReversePolishNotationToken)_ops.Peek();

                                while (IsOperatorToken(opstoken.TokenValueType))
                                {
                                    if (opstoken.TokenValueType == TokenType.Plus || opstoken.TokenValueType == TokenType.Minus)
                                    {
                                        break;
                                    }
                                    else
                                    {
                                        _output.Enqueue(_ops.Pop());
                                        if (_ops.Count > 0)
                                        {
                                            opstoken = (ReversePolishNotationToken)_ops.Peek();
                                        }
                                        else
                                        {
                                            break;
                                        }
                                    }
                                }
                            }

                            _ops.Push(token);
                            break;
                        case "^":
                            token.TokenValueType = TokenType.Exponent;
                            _ops.Push(token);
                            break;
                        case "~":
                            token.TokenValueType = TokenType.UnaryMinus;
                            _ops.Push(token);
                            break;
                        case "(":
                            token.TokenValueType = TokenType.LeftParenthesis;
                            _ops.Push(token);
                            break;
                        case ")":
                            token.TokenValueType = TokenType.RightParenthesis;
                            if (_ops.Count > 0)
                            {
                                opstoken = (ReversePolishNotationToken)_ops.Peek();

                                while (opstoken.TokenValueType != TokenType.LeftParenthesis)
                                {
                                    _output.Enqueue(_ops.Pop());
                                    if (_ops.Count > 0)
                                    {
                                        opstoken = (ReversePolishNotationToken)_ops.Peek();
                                    }
                                    else
                                    {
                                        throw new Exception("Unbalanced parenthesis!");
                                    }

                                }
                                _ops.Pop();
                            }

                            if (_ops.Count > 0)
                            {
                                opstoken = (ReversePolishNotationToken)_ops.Peek();
                            }
                            break;
                    }
                }
            }

            while (_ops.Count != 0)
            {
                opstoken = (ReversePolishNotationToken)_ops.Pop();

                if (opstoken.TokenValueType == TokenType.LeftParenthesis)
                {
                    throw new Exception("Unbalanced parenthesis!");
                }
                else
                {
                    _output.Enqueue(opstoken);
                }
            }

            _sPostfixExpression = string.Empty;
            foreach (object obj in _output)
            {
                opstoken = (ReversePolishNotationToken)obj;
                _sPostfixExpression += string.Format("{0} ", opstoken.TokenValue);
            }
        }

        public double Evaluate()
        {
            Stack result = new Stack();
            double oper1, oper2;
            ReversePolishNotationToken token;

            foreach (object obj in _output)
            {
                token = (ReversePolishNotationToken)obj;
                switch (token.TokenValueType)
                {
                    case TokenType.Number:
                        result.Push(double.Parse(token.TokenValue));
                        break;

                    case TokenType.Plus:
                        if (result.Count >= 2)
                        {
                            oper2 = (double)result.Pop();
                            oper1 = (double)result.Pop();
                            result.Push(oper1 + oper2);
                        }
                        else
                        {
                            throw new Exception("Evaluation error!");
                        }
                        break;
                    case TokenType.Minus:
                        if (result.Count >= 2)
                        {
                            oper2 = (double)result.Pop();
                            oper1 = (double)result.Pop();
                            result.Push(oper1 - oper2);
                        }
                        else
                        {
                            throw new Exception("Evaluation error!");
                        }
                        break;
                    case TokenType.Multiply:
                        if (result.Count >= 2)
                        {
                            oper2 = (double)result.Pop();
                            oper1 = (double)result.Pop();
                            result.Push(oper1 * oper2);
                        }
                        else
                        {
                            throw new Exception("Evaluation error!");
                        }
                        break;
                    case TokenType.Divide:
                        if (result.Count >= 2)
                        {
                            oper2 = (double)result.Pop();
                            oper1 = (double)result.Pop();
                            result.Push(oper1 / oper2);
                        }
                        else
                        {
                            throw new Exception("Evaluation error!");
                        }
                        break;
                    case TokenType.Exponent:
                        if (result.Count >= 2)
                        {
                            oper2 = (double)result.Pop();
                            oper1 = (double)result.Pop();
                            result.Push(Math.Pow(oper1, oper2));
                        }
                        else
                        {
                            throw new Exception("Evaluation error!");
                        }
                        break;
                    case TokenType.UnaryMinus:
                        if (result.Count >= 1)
                        {
                            oper1 = (double)result.Pop();
                            result.Push(-oper1);
                        }
                        else
                        {
                            throw new Exception("Evaluation error!");
                        }
                        break;
                }
            }

            if (result.Count == 1)
            {
                return Math.Round((double)result.Pop(), 10);
            }
            else
            {
                throw new Exception("Evaluation error!");
            }
        }

        private bool IsOperatorToken(TokenType t)
        {
            bool result;
            switch (t)
            {
                case TokenType.Plus:
                case TokenType.Minus:
                case TokenType.Multiply:
                case TokenType.Divide:
                case TokenType.Exponent:
                case TokenType.UnaryMinus:
                    result = true;
                    break;
                default:
                    result = false;
                    break;
            }
            return result;
        }
    }
}
