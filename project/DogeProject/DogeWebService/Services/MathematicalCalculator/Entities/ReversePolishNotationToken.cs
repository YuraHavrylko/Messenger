﻿namespace DogeWebService.Services.MathematicalCalculator.Entities
{
    public struct ReversePolishNotationToken
    {
        public string TokenValue;
        public TokenType TokenValueType;
    }
}
