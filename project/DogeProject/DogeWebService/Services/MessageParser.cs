﻿using System;
using System.Text.RegularExpressions;
using DogeWebService.Contracts.Services;
using DogeWebService.Contracts.Services.MathematicalCalculator;
using DogeWebService.Contracts.Services.Web;
using Microsoft.Practices.Unity;

namespace DogeWebService.Services
{
    public class MessageParser : IMessageParser
    {
        string PatternUrl = @"(https?|ftp):\/\/(-\.)?([^\s/?\.#-]+\.?)+(\/[^\s]*)?";

        string PatternEmail =
            @"(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|'(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*')@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])";

        string PatternMath = @"[-+]?(( ?)(\(*)+[-+]?[0-9]*\.?[0-9]+( ?)+(\)*)+( ?)+[\/\+\-\*])+( ?)+(\(*)+([-+]?[0-9]*\.?[0-9]+(\)*))";

        string PatternWhatIs = @"(?<=what is )(.*)";


        private IMathematicalCalculator _calculator;
        private IWhatIsCommand _whatIs;

        private ITagCreator _linkTagCreator;
        private ITagCreator _emailTagCreator;
        
        public MessageParser(IMathematicalCalculator calculator, 
                IWhatIsCommand whatIs, 
                [Dependency("LinkTagCreator")] ITagCreator linkTagCreator,
                [Dependency("EmailTagCreator")] ITagCreator emailTagCreator)
        {
            _calculator = calculator;
            _whatIs = whatIs;

            _linkTagCreator = linkTagCreator;
            _emailTagCreator = emailTagCreator;
        }

        public string ParseUrl(string message)
        {
            var regex = new Regex(PatternUrl, RegexOptions.IgnoreCase);

            MatchCollection collection = regex.Matches(message);
            if (collection.Count > 0)
            {
                foreach (var item in collection)
                {
                    message = message.Replace(item.ToString(), _linkTagCreator.CreateTagLink(item.ToString()));
                }
            }
            return message;
        }

        public string ParseEmail(string message)
        {
            var regex = new Regex(PatternEmail);

            MatchCollection collection = regex.Matches(message);
            if (collection.Count > 0)
            {
                foreach (var item in collection)
                {
                    message = message.Replace(item.ToString(), _emailTagCreator.CreateTagLink(item.ToString()));
                }
            }
            return message;
        }

        public string ParseMath(string message)
        {
            var regex = new Regex(PatternMath);
            MatchCollection collection = regex.Matches(message);            
            if (collection.Count > 0)
            {
                foreach (var item in collection)
                {
                    try
                    {
                        _calculator.Parse(item.ToString());
                        message = message.Replace(item.ToString(), "" + _calculator.Evaluate());
                    }
                    catch (Exception)
                    {
                        message = message.Replace(item.ToString(), item + "(Cannot calculate)");
                    }
                    
                    
                }
            }
            return message;
        }

        public string ParseWhatIs(string message)
        {
            var regex = new Regex(PatternWhatIs, RegexOptions.IgnoreCase);
            MatchCollection collection = regex.Matches(message);
            string newMessage = "";
            
            if (collection.Count > 0)
            {
                foreach (var item in collection)
                {
                    try
                    {
                        newMessage += String.Format("{0} is...\n {1} \n", item, _whatIs.WhatIs(item.ToString()));
                    }
                    catch (Exception e)
                    {
                        
                        newMessage += String.Format("{0} is...\n {1} \n", item, e.Message);
                    }
                    
                }
            }
            return newMessage;
        }
    }
}