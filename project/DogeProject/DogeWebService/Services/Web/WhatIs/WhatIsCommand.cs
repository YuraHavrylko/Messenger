﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using DataContracts.Entities;
using DogeWebService.Contracts.Models.Services;
using DogeWebService.Contracts.Services.Web;
using HtmlDocument = HtmlAgilityPack.HtmlDocument;


namespace DogeWebService.Services.Web.WhatIs
{
    public class WhatIsCommand : IWhatIsCommand
    {
        private readonly IWhatIsDataLayer _dataLayer;

        public WhatIsCommand(IWhatIsDataLayer dataLayer)
        {
            _dataLayer = dataLayer;
        }

        public string WhatIs(string findWord)
        {
            WhatIsArticle article = _dataLayer.GetArticle(findWord);

            if (article.articleName != null)
            {
                return article.articleParagraph;
            }
            else
            {
                var doc = new HtmlDocument();
                doc.LoadHtml(GetRequest("https://wikipedia.org/wiki/" + findWord));
                var node = doc.DocumentNode.SelectSingleNode("//*[@id='mw-content-text']/p[1]");

                if (node == null) throw new Exception("Try enter another data");
                var str = node.OuterHtml;
                var htmlNew = Regex.Replace(str, "<[^>]+>", string.Empty);

                _dataLayer.AddNewArticle(findWord, htmlNew);

                return htmlNew;

            }
        }

        private static string GetRequest(string url)
        {
            try
            {
                var httpWebRequest = WebRequest.Create(url) as HttpWebRequest;

                using (var httpWebResponse = httpWebRequest?.GetResponse() as HttpWebResponse)
                {
                    using (var stream = httpWebResponse?.GetResponseStream())
                    {
                        using (var reader = new StreamReader(stream, Encoding.GetEncoding(httpWebResponse.CharacterSet)))
                        {
                            return reader.ReadToEnd();
                        }
                    }
                }
            }
            catch
            {
                return string.Empty;
            }
        }
    }
}

