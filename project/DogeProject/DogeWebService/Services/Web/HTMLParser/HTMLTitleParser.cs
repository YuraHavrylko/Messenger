﻿using System.Text.RegularExpressions;
using DogeWebService.Contracts.Services.Web;

namespace DogeWebService.Services.Web.HTMLParser
{
    public class HtmlTitleParser : IHtmlParser
    {
        public const string RegexPattern = @"\<title\b[^>]*\>\s*(?<Title>[\s\S]*?)\</title\>";

        public string HTMLCode { get; set; }

        public HtmlTitleParser(string htmlCode)
        {
            HTMLCode = htmlCode;
        }
        
        public string Parse()
        {
            return Regex.Match(HTMLCode, RegexPattern, 
                RegexOptions.IgnoreCase).Groups["Title"].Value;
        }
    }
}
