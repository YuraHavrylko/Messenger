﻿using System;
using System.Net;
using System.Text;
using DogeWebService.Contracts.Services.Web;
using DogeWebService.Services.Web.HTMLParser;

namespace DogeWebService.Services.Web.TagCreators
{
    public class LinkTagCreator : ITagCreator
    {
        public const string TagFormatLink = "<a href=\"{0}\">{1}</a>";
       

        private IHtmlParser parser;

        public string CreateTagLink(string source)
        {
            // Todo: remove getting title of page from this method. BS
            // Todo: add try/catch block. BS
            // Todo: add encoding. BS
            WebClient webClient = new WebClient();
            webClient.Encoding = Encoding.UTF8;
            string pageTitle = "";
            try
            {
                string page = webClient.DownloadString(source);
                parser = new HtmlTitleParser(page);
                pageTitle = parser.Parse();
            }
            catch (Exception)
            {
                pageTitle = source;
            }
            return String.Format(TagFormatLink, source, pageTitle);
        }
    }
}
