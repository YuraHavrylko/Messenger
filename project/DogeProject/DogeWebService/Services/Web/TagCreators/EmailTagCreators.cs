﻿using System;
using DogeWebService.Contracts.Services.Web;

namespace DogeWebService.Services.Web.TagCreators
{
    public class EmailTagCreators :  ITagCreator
    {
        public const string TagFormatEmail = "<a href=\"mailto:{0}\">{1}</a>";

        public string CreateTagLink(string source)
        {
            return String.Format(TagFormatEmail, source, source);
        }
    }
}