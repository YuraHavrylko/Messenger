﻿namespace DogeWebService.Contracts.Services
{
    public interface IMessageParser
    {
        string ParseUrl(string message);
        string ParseEmail(string message);
        string ParseMath(string message);
        string ParseWhatIs(string message);
    }
}