﻿namespace DogeWebService.Contracts.Services.MathematicalCalculator
{
    public interface IMathematicalCalculator
    {
        /// <summary>
        /// Contains original expressions
        /// </summary>
        string OriginalExpression { get; }

        /// <summary>
        /// Contains expressions wtih separated tokens
        /// </summary>
        string TransitionExpression { get; }

        /// <summary>
        /// Contains postfix expression (Reverse Polish notation (RPN))
        /// </summary>
        string PostfixExpression { get; }

        /// <summary>
        /// Converts infix expression to postfix expression
        /// </summary>
        /// <param name="expression">Infix expression to convert</param>
        void Parse(string expression);

        /// <summary>
        /// Evaluate postfix expression from left to right
        /// </summary>
        /// <returns>Evaluation result</returns>
        double Evaluate();
    }
}
