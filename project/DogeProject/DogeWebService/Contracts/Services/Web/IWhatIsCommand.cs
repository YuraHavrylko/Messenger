﻿namespace DogeWebService.Contracts.Services.Web
{
    public interface IWhatIsCommand
    {
        string WhatIs(string findWord);
    }
}
