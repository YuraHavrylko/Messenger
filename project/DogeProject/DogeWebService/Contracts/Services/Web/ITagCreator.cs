﻿namespace DogeWebService.Contracts.Services.Web
{
    public interface ITagCreator
    {
        string CreateTagLink(string source);
    }
}
