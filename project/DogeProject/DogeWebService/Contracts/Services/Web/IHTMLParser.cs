﻿namespace DogeWebService.Contracts.Services.Web
{
    public interface IHtmlParser
    {
        string HTMLCode { get; set; }

        /// <summary>
        /// Getting content from tags
        /// </summary>
        /// <returns>Tag content</returns>
        string Parse();
    }
}
