﻿namespace DogeWebService.Contracts.Services
{
    public interface IUserChecker
    {
        /// <summary>
        /// Returns true if user exist in database, or false if user is missing in database
        /// </summary>
        /// <param name="userName">Current user</param>
        bool CheckUserExistence(string userName);

        /// <summary>
        /// Returns true if user exist in database, or false if user is missing in database
        /// </summary>
        /// <param name="id">Current user</param>
        bool CheckUserExistenceById(int id);

        /// <summary>
        /// Throws exception if at list one user doesn't exist
        /// </summary>
        /// <param name="userName1"></param>
        /// <param name="userName2"></param>
        void CheckBothUsersExistenceByName(string userName1, string userName2);

        /// <summary>
        /// Throws exception if at list one user doesn't exist
        /// </summary>
        /// <param name="userId1"></param>
        /// <param name="userId2"></param>
        void CheckBothUsersExistenceById(int userId1, int userId2);
    }
}
