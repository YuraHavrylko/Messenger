﻿using DataContracts.Entities;

namespace DogeWebService.Contracts.Models
{
    public interface IBotDal
    {
        int GetNumberOfPhrases();
        BotPhrase GetPhraseById(int id);
    }
}