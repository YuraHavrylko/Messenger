﻿using System;
using System.Collections.Generic;
using DataContracts.Entities;

namespace DogeWebService.Contracts.Models
{
    public interface IChatDal
    {
        void AddMessageToDb(int userFromId, int userToId, string originalMessage, string modifiedMessage, DateTime timeOfSending);
        void DeleteUsersMessages(int userId);
        List<Message> GetDialog(int userFromId, int userToId);
        List<Message> GetDialogFromDate(int userFromId, int userToId, DateTime fromDate);
        List<Message> GetAllUserDialogs(int id);
    }
}