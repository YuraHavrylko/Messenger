﻿using System.Data;

namespace DogeWebService.Contracts.Models.Infrastructure
{
    public interface IConnectionFactory
    {
        IDbConnection Create();
    }
}
