﻿using System.Collections.Generic;
using DataContracts.Entities;

namespace DogeWebService.Contracts.Models
{
    public interface IUserFriendshipDal
    {
        List<Connection> GetConnections();
        List<UserMetadata> GetUsersFriends(int currentUser);

        UserMetadata GetUserById(int id);
    }
}