﻿namespace DogeWebService.Contracts.Models.Services
{
    public interface IUserCheckerDal
    {
        bool CheckUserExistence(string currentUser);
        bool CheckUserExistenceById(int id);
    }
}
