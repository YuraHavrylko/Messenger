﻿using DataContracts.Entities;

namespace DogeWebService.Contracts.Models.Services
{
    public interface IWhatIsDataLayer
    {
        void AddNewArticle(string articleName, string articleParagraph);
        WhatIsArticle GetArticle(string articleName);
    }
}
