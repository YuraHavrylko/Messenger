﻿using System.Collections.Generic;
using DataContracts.Entities;

namespace DogeWebService.Contracts.Models
{
    public interface IDataBaseLayer
    {
        List<User> GetUsers();
        User GetUser(string userName);
        void AddUserToDb(string userName, string password);
        void DeleteUserFromDb(string userName);

        void AddMessageToDb(int userFrom, int UserTo, string originalMessage, string modifiedMessage);
        void DeleteUsersMessages(int currentUser);
        List<Message> GetDialog(int currentUserId, int secondUserId);

        bool CheckUserData(string userName, string password);
        bool CheckUserExistence(string currentUser);
        List<User> GetUsersFriends(int currentUser);
        
        int GetNumberOfPhrases();
        BotPhrase GetPhraseById(int id);
        List<Connection> GetConnections();

        void SetCookie(string userName, string cookie);
        string GetCookie(string userName);
    }
}