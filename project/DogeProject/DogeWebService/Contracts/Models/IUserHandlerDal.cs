﻿using System.Collections.Generic;
using DataContracts.Entities;

namespace DogeWebService.Contracts.Models
{
    public interface IUserHandlerDal
    {
        List<User> GetUsers();

        User GetLoginAndPass(int id);

        UserMetadata GetUserById(int id);

        UserMetadata GetUserByUserName(string userName);

        List<UserMetadata> SearchUser(string userName);

        void AddUserToDb(string userName, string password);

        int DeleteUserFromDb(int id);

        bool CheckUserData(string userName, string password);
    }
}