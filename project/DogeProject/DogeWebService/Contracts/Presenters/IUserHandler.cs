﻿using System.Collections.Generic;
using System.ServiceModel;
using DataContracts.Entities;

namespace DogeWebService.Contracts.Presenters
{
    [ServiceContract]
    public interface IUserHandler
    {
        /// <summary>
        /// Returns all users from database
        /// </summary>
        [OperationContract]
        List<User> GetUsers();

        [OperationContract]
        UserMetadata GetUserById(int id);

        [OperationContract]
        UserMetadata GetUserByUserName(string userName);

        [OperationContract]
        List<UserMetadata> SearchUser(string userName);

        /// <summary>
        /// Adds a new user to the database
        /// </summary>
        /// <param name="userName">New user name</param>
        /// <param name="password">New user password</param>
        [OperationContract]
        void AddUser(string userName, string password);

        /// <summary>
        /// Removes the user, all connections and messages with him from the database
        /// </summary>
        /// <param name="userId">Name of the removed user</param>
        [OperationContract]
        void DeleteUser(int userId);

        /// <summary>
        /// Returns true if user exist in database and if password is correct, else returns false
        /// </summary>
        /// <param name="userName">Current user</param>
        /// <param name="password">Current password</param>
        [OperationContract]
        bool Login(string userName, string password);

        /// <summary>
        /// Returns true if user exist in database, or false if user is missing in database
        /// </summary>
        /// <param name="userName">Current user</param>
        [OperationContract]
        bool CheckUserExistence(string userName);

        /// <summary>
        /// Returns true if user exist in database, or false if user is missing in database
        /// </summary>
        /// <param name="id">Current user</param>
        [OperationContract]
        bool CheckUserExistenceById(int id);

        /// <summary>
        /// Throws exception if at list one user doesn't exist
        /// </summary>
        /// <param name="userName1"></param>
        /// <param name="userName2"></param>
        [OperationContract]
        void CheckBothUsersExistenceByName(string userName1, string userName2);
    }
}
