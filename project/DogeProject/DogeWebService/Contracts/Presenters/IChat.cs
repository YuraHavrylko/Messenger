﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using DataContracts.Entities;

namespace DogeWebService.Contracts.Presenters
{
    [ServiceContract]
    public interface IChat
    {
        /// <summary>
        /// Sending the message ot another user. 
        /// If connection between users was not, creates a new connection
        /// </summary>
        /// <param name="userId1">User-sender</param>
        /// <param name="userId2">User-receiver</param> 
        /// <param name="message">Text of message</param> 
        [OperationContract]
        void WriteMessage(int userId1, int userId2, string message);

        /// <summary>
        /// Returns dialog with selected user
        /// </summary>
        /// <param name="userId1">Current user</param>
        /// <param name="userId2">Selected user</param>
        /// <returns></returns>
        [OperationContract]
        List<Message> GetDialog(int userId1, int userId2);

        /// <summary>
        /// Returns dialog with selected user
        /// </summary>
        /// <param name="userId1">Current user</param>
        /// <param name="userId2">Selected user</param>
        /// <param name="fromDate">Date from which messages are being selected</param>
        /// <returns></returns>
        [OperationContract]
        List<Message> GetDialogFromDate(int userId1, int userId2, int fromDate);

        [OperationContract]
        List<Message> GetAllUserDialogs(int userId);
    }
}
