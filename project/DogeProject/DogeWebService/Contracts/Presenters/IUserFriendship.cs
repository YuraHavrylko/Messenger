﻿using System.Collections.Generic;
using System.ServiceModel;
using DataContracts.Entities;

namespace DogeWebService.Contracts.Presenters
{
    [ServiceContract]
    public interface IUserFriendship
    {
        /// <summary>
        /// Returns list of all friends of current user
        /// </summary>
        /// <param name="currentUser">Current user</param>
        [OperationContract]
        List<UserMetadata> GetUsersFriends(int userId);

        /// <summary>
        /// Returns the distance of friends between current users
        /// </summary>
        /// <param name="userId1">Id of first user</param>
        /// <param name="userId2">Id of second user</param> 
        [OperationContract]
        int FindPath(int userId1, int userId2);


        /// <summary>
        /// Returns the list of common friends between current users
        /// </summary>
        /// <param name="userId1">Id of first user</param>
        /// <param name="userId2">Id of second user</param> 
        [OperationContract]
        List<UserMetadata> FindCommonFriends(int userId1, int userId2);


        /// <summary>
        /// Returns all path of friends between current users
        /// </summary>
        /// <param name="userId1">Id of first user</param>
        /// <param name="userId2">Id of second user</param> 
        [OperationContract]
        List<UserMetadata> FindPathWithResultData(int userId1, int userId2);
    }
}