﻿using System.ServiceModel;

namespace DogeWebService.Contracts.Presenters
{
    [ServiceContract]
    public interface IBot
    {
        /// <summary>
        /// Processes the messages, detects url, mail, math and that is in this
        /// </summary>
        /// <param name="_message">Sent message</param>
        [OperationContract]
        string MessageHandler(string message);
    }
}
