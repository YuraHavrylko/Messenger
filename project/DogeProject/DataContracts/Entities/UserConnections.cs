﻿using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace DataContracts.Entities
{
    [DataContract(Namespace = "DataContracts.Entities")]
    [StructLayout(LayoutKind.Sequential)]
    public struct UserConnections
    {
        [DataMember]
        public int connectionId;
        [DataMember]
        public string userName1;
        [DataMember]
        public string userName2;
    }
}
