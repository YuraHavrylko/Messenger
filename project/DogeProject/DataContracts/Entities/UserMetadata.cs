﻿using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace DataContracts.Entities
{
    [StructLayout(LayoutKind.Sequential)]
    [DataContract(Namespace = "DataContracts.Entities")]
    public struct UserMetadata
    {
        [DataMember]
        public int userId;
        [DataMember]
        public string userName;

        public UserMetadata(int userId, string userName)
        {
            this.userId = userId;
            this.userName = userName;
        }
    }
}
