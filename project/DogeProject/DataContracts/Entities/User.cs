﻿using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace DataContracts.Entities
{
    [StructLayout(LayoutKind.Sequential)]
    [DataContract(Namespace = "DataContracts.Entities")]
    public struct User
    {
        [DataMember]
        public int userId;
        [DataMember]
        public string userName;
        [DataMember]
        public string password;

        public User(int userId, string userName, string password)
        {
            this.userId = userId;
            this.userName = userName;
            this.password = password;
        }
    }
}
