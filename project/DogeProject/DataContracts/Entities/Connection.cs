﻿using System.Runtime.Serialization;

namespace DataContracts.Entities
{
    [DataContract(Namespace = "DataContracts.Entities")]
    public struct Connection
    {
        [DataMember]
        public int fromUser;
        [DataMember]
        public int toUser;
    }
}
