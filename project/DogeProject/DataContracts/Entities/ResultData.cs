﻿using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace DataContracts.Entities
{
    [DataContract(Namespace = "DataContracts.Entities")]
    [StructLayout(LayoutKind.Sequential)]
    public class ResultData
    {
        [DataMember]
        public int userNumberInPath;
        [DataMember]
        public int userId;
    }
}