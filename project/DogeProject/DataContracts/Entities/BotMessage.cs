﻿using System.Runtime.Serialization;

namespace DataContracts.Entities
{
    [DataContract(Namespace = "DataContracts.Entities")] 
    public struct BotPhrase
    {
        [DataMember]
        public string phrase;
    }
}
