﻿using System;
using System.Runtime.Serialization;

namespace DataContracts.Entities
{
    [DataContract(Namespace = "DataContracts.Entities")]
    public struct Message
    {
        [DataMember]
        public int messageId;
        [DataMember]
        public string fromUser;
        [DataMember]
        public string toUser;
        [DataMember]
        public string message;
        [DataMember]
        public DateTime timeOfSending;
    }
}
