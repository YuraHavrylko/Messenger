﻿using System.Runtime.Serialization;

namespace DataContracts.Entities
{
    [DataContract(Namespace = "DataContracts.Entities")]
    public struct WhatIsArticle
    {
        [DataMember]
        public string articleName;
        [DataMember]
        public string articleParagraph;
    }
}
