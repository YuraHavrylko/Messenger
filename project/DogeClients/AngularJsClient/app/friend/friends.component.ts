import {Component} from "@angular/core";
import {UserMetadata} from "./usermetadata";
import {Message} from "../messages/message";
import {CustomFilter} from "./customfilter";
import {Http, HTTP_PROVIDERS, Headers, Response, Request, RequestOptions} from '@angular/http';
@Component({
    selector: "friends",
    templateUrl: "./app/friend/friends.component.html",
    pipes: [CustomFilter],
})
export class FriendsComponent {

    resultData: UserMetadata[];
    users: UserMetadata[];
    httpService: Http;
    response: string;
    countPath: number;
    info: string;
    friend: UserMetadata;
    currUser: UserMetadata;
    targetUser: UserMetadata;
    message: string;
    findWord: string;
    
    constructor($http: Http) {
        this.httpService = $http;
        this.getFriends();
        this.getCurrentUser();
        this.targetUser = new UserMetadata();
        this.targetUser.userName = '';
        this.findWord = '';
    }

    getFriends(): void {
        this.info = 'Friends';
        var headers = new Headers();
        headers.append('Content-Type', 'application/json; charset=utf-8');
        headers.append('Authorization', sessionStorage.getItem('auth-token-type') + ' ' + sessionStorage.getItem('auth-token-access-token'));
        this.httpService.get(
            'http://localhost:49768/api/FriendShip/GetCurrentUserFriends',
            new RequestOptions({ headers: headers }))
            .subscribe(
            //succses
            (data) => {
                this.users = JSON.parse(data.text())
            },
            // error
            (error) => {
                window.location.assign('/Login');
            }
            );
    }

    getCommonFriends(user: UserMetadata): void {
        this.info = 'Common friends with user ' + user.userName;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json; charset=utf-8');
        headers.append('Authorization', sessionStorage.getItem('auth-token-type') + ' ' + sessionStorage.getItem('auth-token-access-token'));
        this.httpService.get(
            'http://localhost:49768/api/FriendShip/GetCommonFriends/' + user.userId,
            new RequestOptions({ headers: headers }))
            .subscribe(
            //succses
            (data) => {
                this.users = JSON.parse(data.text())
            },
            // error
            (error) => {
                window.location.assign('/Login');
            }
            );
    }

    getUserFriend(user: UserMetadata): void {
        this.info = 'Friends of user ' + user.userName;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json; charset=utf-8');
        headers.append('Authorization', sessionStorage.getItem('auth-token-type') + ' ' + sessionStorage.getItem('auth-token-access-token'));
        this.httpService.get(
            'http://localhost:49768/api/FriendShip/GetUserFriends/' + user.userId,
            new RequestOptions({ headers: headers }))
            .subscribe(
            //succses
            (data) => {
                this.users = JSON.parse(data.text())
            },
            // error
            (error) => {
                window.location.assign('/Login');
            }
            );
    }

    getPathToFriend(user: UserMetadata): void {
        this.friend = user;
        this.resultData = null;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json; charset=utf-8');
        headers.append('Authorization', sessionStorage.getItem('auth-token-type') + ' ' + sessionStorage.getItem('auth-token-access-token'));
        this.httpService.get(
            'http://localhost:49768/api/FriendShip/GetCountPath/' + user.userId,
            new RequestOptions({ headers: headers }))
            .subscribe(
            //succses
            (data) => {
                this.countPath = JSON.parse(data.text())
            },
            // error
            (error) => {
                window.location.assign('/Login');
            }
            );
    }

    getPathToFriendWithResultData(user: UserMetadata): void {
        var headers = new Headers();

        headers.append('Content-Type', 'application/json; charset=utf-8');
        headers.append('Authorization', sessionStorage.getItem('auth-token-type') + ' ' + sessionStorage.getItem('auth-token-access-token'));
        this.httpService.get(
            'http://localhost:49768/api/FriendShip/GetCountAndFriendsPath/' + this.friend.userId,
            new RequestOptions({ headers: headers }))
            .subscribe(
            //succses
            (data) => {
                this.resultData = JSON.parse(data.text())
            },
            // error
            (error) => {
                window.location.assign('/Login');
            }
            );
    }

    getUsers(): void {
        this.info = 'All users';
        var headers = new Headers();
        headers.append('Content-Type', 'application/json; charset=utf-8');
        headers.append('Authorization', sessionStorage.getItem('auth-token-type') + ' ' + sessionStorage.getItem('auth-token-access-token'));
        this.httpService.get(
            'http://localhost:49768/api/User/GetUsers',
            new RequestOptions({ headers: headers }))
            .subscribe(
            //succses
            (data) => {
                this.users = JSON.parse(data.text())
            },
            // error
            (error) => {
                window.location.assign('/Login');
            }
            );
    }

    getCurrentUser(): void {
        var headers = new Headers();
        headers.append('Content-Type', 'application/json; charset=utf-8');
        headers.append('Authorization', sessionStorage.getItem('auth-token-type') + ' ' + sessionStorage.getItem('auth-token-access-token'));
        this.httpService.get(
            'http://localhost:49768/api/User/GetCurrentUser',
            new RequestOptions({ headers: headers }))
            .subscribe(
            //succses
            (data) => {
                this.currUser = JSON.parse(data.text())
            },
            // error
            (error) => {
                window.location.assign('/Login');
            }
            );

    }

    sendMessage(): void {

        var headers = new Headers();
        headers.append('Content-Type', 'application/json; charset=utf-8');
        headers.append('Authorization', sessionStorage.getItem('auth-token-type') + ' ' + sessionStorage.getItem('auth-token-access-token'));
        var message = new Message();
        message.fromUser = this.currUser.userName;
        message.toUser = this.targetUser.userName;
        message.message = this.message;
        this.message = '';
        this.httpService.post(
            'http://localhost:49768/api/Message/SendMessage/',
            JSON.stringify(message),
            new RequestOptions({ headers: headers }))
            .subscribe(
            //succses
            (data) => {
            },
            // error
            (error) => {
                window.location.assign('/Login');
            }
            );
    }

    setTargetUser(user: UserMetadata) {
        this.targetUser = user;
    }

}