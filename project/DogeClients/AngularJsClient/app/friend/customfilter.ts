import {Injectable, Pipe, PipeTransform} from "@angular/core";
import {UserMetadata} from "./usermetadata";

@Pipe({
  name: 'nameFilter'
})
@Injectable()
export class CustomFilter implements PipeTransform {
  transform(users: UserMetadata[], args: string): any {
    if(args.length == 0)
      return users;
    return users.filter(users => users.userName.toLowerCase().search(args.toLowerCase()) !== -1);
  }
}