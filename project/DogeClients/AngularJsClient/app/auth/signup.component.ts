import {Component} from "@angular/core";
import {UserCredentional} from "./usercredentional";
import {Http, HTTP_PROVIDERS, Headers, Response, Request, RequestOptions} from '@angular/http';
@Component({
    selector: "register",
    templateUrl: "./app/auth/signup.component.html"
})
export class UserRegistrationComponent {
    constructor($http: Http) {
        this.httpService = $http;
    }

    user: UserCredentional = new UserCredentional();
    httpService: Http;
    response: string;
    confirmPassword: string;
    addUser(): void {
        console.log(this.user.Password);
        console.log(this.confirmPassword);
        
        if (this.confirmPassword !== this.user.Password) {
            this.response = 'Password and confirm password not equals';
        }
        else {

            var headers = new Headers();
            headers.append('Content-Type', 'application/json; charset=utf-8');
            this.httpService.post(
                'http://localhost:49768/api/Account/SignUp',
                JSON.stringify(this.user),
                new RequestOptions({ headers: headers }))
                .subscribe(
                //succses
                (data) => {
                    this.response = JSON.parse(data.text());
                    window.location.assign('/Login');
                },
                // error
                (error) => {
                    this.response = JSON.parse(error.text()).exceptionMessage;
                    this.user.Password = '';
                    this.confirmPassword = '';
                }
                );

        }
    }
}