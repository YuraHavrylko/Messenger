import {Component} from "@angular/core";
import {UserCredentional} from "./usercredentional";
import {UserRegistrationComponent} from "./signup.component";
import {RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS } from '@angular/router-deprecated';
import {LoginResponse} from "./loginresponse";
import {LoginError} from "./loginerror";
import {Http, HTTP_PROVIDERS, Headers, Response, Request, RequestOptions} from '@angular/http';


@Component({
    selector: "login",
    templateUrl: "./app/auth/signin.component.html",
    directives: [UserRegistrationComponent],
})
export class UserLoginComponent {
    constructor($http: Http) {
        this.httpService = $http;
    }

    httpService: Http;
    context: LoginResponse;
    error: LoginError;
    response: string;
    user: UserCredentional = new UserCredentional();
    status: string;
    loginUser(): void {
        var headers = new Headers();
        headers.append('Content-Type', 'x-www-form-urlencoded; charset=utf-8');
        this.httpService.post(
            'http://localhost:49768/api/Auth/Token',
            'grant_type=password&username=' + this.user.UserName + '&password=' + this.user.Password,
            new RequestOptions({ headers: headers }))
            .subscribe(
            //succses
            (data) => {
                this.context = JSON.parse(data.text());
                sessionStorage.setItem('auth-token-access-token', this.context.access_token);
                sessionStorage.setItem('auth-token-type', this.context.token_type);
                sessionStorage.setItem('auth-token-expires-in', this.context.expires_in);
                sessionStorage.setItem('auth-token-refresh-token', this.context.refresh_token);
                this.status = "You login";
                window.location.assign('/Friends');
            },
            // error
            (error) => {
                this.user.Password = '';
                this.status = JSON.parse(error.text()).error_description;
                sessionStorage.setItem('auth-token-access-token', '');
                sessionStorage.setItem('auth-token-type', '');
                sessionStorage.setItem('auth-token-expires-in', '');
                sessionStorage.setItem('auth-token-refresh-token', '');
            }

            );
    }
}