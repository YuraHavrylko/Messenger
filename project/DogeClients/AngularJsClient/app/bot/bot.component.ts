import {Component} from "@angular/core";
import {BotData} from "./botdata";
import {Http, HTTP_PROVIDERS, Headers, Response, Request, RequestOptions} from '@angular/http';
@Component({
    selector: "friends",
    templateUrl: "./app/bot/bot.component.html"
})
export class BotComponent {

    messages: Array<BotData>;
    message: string;
    httpService: Http;
    constructor($http: Http) {
        if (sessionStorage.getItem('auth-token-access-token') == '') {
            window.location.assign('/Login');
        }
        this.messages = new Array<BotData>();
        this.httpService = $http;
    }

    sendBotMessage(): void {

        var headers = new Headers();
        var item = new BotData();
        item.userName = 'Me';
        item.message = this.message;
        this.messages.push(item);
        headers.append('Content-Type', 'x-www-form-urlencoded; charset=utf-8');
        headers.append('Authorization', sessionStorage.getItem('auth-token-type') + ' ' + sessionStorage.getItem('auth-token-access-token'));
        this.httpService.post(
            'http://localhost:49768/api/Message/SendBotMessage/',
            this.message,
            new RequestOptions({ headers: headers }))
            .subscribe(
            //succses
            (data) => {
                this.message = '';
                var item = new BotData();
                item.userName = 'Bot';
                item.message = data.text();
                this.messages.push(item);
            },
            // error
            (error) => {
                window.location.assign('/Login');
            }
            );
    }


}