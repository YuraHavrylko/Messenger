import {Component} from "@angular/core";
import {MessageMetadata} from "./messagesmetadata";
import {UserMetadata} from "../friend/usermetadata";
import {Message} from "./message";
import {Http, HTTP_PROVIDERS, Headers, Response, Request, RequestOptions} from '@angular/http';
@Component({
    selector: "messages",
    templateUrl: "./app/messages/messages.component.html"
})
export class MessagesComponent {

    messages: MessageMetadata[];
    httpService: Http;
    response: string;
    countPath: number;
    dialogs: MessageMetadata[];

    user1: string;
    user2: string;
    currUser: UserMetadata;

    message: string;
    static timerConcreteDialog: NodeJS.Timer;
    static timerCurrentDialogs: NodeJS.Timer;

    constructor($http: Http) {
        this.user1 = undefined;
        this.user2 = undefined;
        this.httpService = $http;
        this.getCurrentUser();
        this.getCurrentDialogs();
        clearInterval(MessagesComponent.timerCurrentDialogs);
        clearInterval(MessagesComponent.timerConcreteDialog);
        MessagesComponent.timerCurrentDialogs = setInterval(() => {
            this.getCurrentDialogs();
        }, 5000);
    }

    getCurrentDialogs(): void {
        var headers = new Headers();
        headers.append('Content-Type', 'application/json; charset=utf-8');
        headers.append('Authorization', sessionStorage.getItem('auth-token-type') + ' ' + sessionStorage.getItem('auth-token-access-token'));
        this.httpService.get(
            'http://localhost:49768/api/Message/GetCurrentDialogs',
            new RequestOptions({ headers: headers }))
            .subscribe(
            //succses
            (data) => {
                this.messages = JSON.parse(data.text())
                this.getCurrentUser()
                this.messages.reverse()
                for (var index = 0; index < this.messages.length; index++) {
                    if (this.messages[index].fromUser === this.currUser.userName) {
                        var temp = this.messages[index].fromUser
                        this.messages[index].fromUser = this.messages[index].toUser
                        this.messages[index].toUser = temp
                        this.messages[index].message = "You: " + this.messages[index].message
                    }
                }
                clearInterval(MessagesComponent.timerCurrentDialogs);
                MessagesComponent.timerCurrentDialogs = setInterval(() => {
                    this.getCurrentDialogs();
                }, 5000);
            },
            // error
            (error) => {
                window.location.assign('/Login');
            }
            );
    }

    getDialog(userFrom: string, userTo: string): void {
        this.user1 = userFrom
        this.user2 = userTo
        var headers = new Headers();

        headers.append('Content-Type', 'application/json; charset=utf-8');
        headers.append('Authorization', sessionStorage.getItem('auth-token-type') + ' ' + sessionStorage.getItem('auth-token-access-token'));
        this.httpService.get(
            'http://localhost:49768/api/Message/GetDialog/' + userFrom + '/' + userTo,
            new RequestOptions({ headers: headers }))
            .subscribe(
            //succses
            (data) => {
                this.dialogs = JSON.parse(data.text())
                clearInterval(MessagesComponent.timerConcreteDialog);
                MessagesComponent.timerConcreteDialog = setInterval(() => {
                    this.getDialog(this.user1, this.user2);
                }, 5000);
            },
            // error
            (error) => {
                window.location.assign('/Login');
            }
            );

    }

    sendMessage(): void {

        var headers = new Headers();
        headers.append('Content-Type', 'application/json; charset=utf-8');
        headers.append('Authorization', sessionStorage.getItem('auth-token-type') + ' ' + sessionStorage.getItem('auth-token-access-token'));
        var message = new Message();
        message.fromUser = this.user1;
        message.toUser = this.user2;
        message.message = this.message;
        this.message = '';
        this.httpService.post(
            'http://localhost:49768/api/Message/SendMessage/',
            JSON.stringify(message),
            new RequestOptions({ headers: headers }))
            .subscribe(
            //succses
            (data) => {

                this.getDialog(this.user1, this.user2)
                this.getCurrentDialogs()
            },
            // error
            (error) => {
                window.location.assign('/Login');

            }
            );
    }

    getCurrentUser(): void {
        var headers = new Headers();
        headers.append('Content-Type', 'application/json; charset=utf-8');
        headers.append('Authorization', sessionStorage.getItem('auth-token-type') + ' ' + sessionStorage.getItem('auth-token-access-token'));
        this.httpService.get(
            'http://localhost:49768/api/User/GetCurrentUser',
            new RequestOptions({ headers: headers }))
            .subscribe(
            //succses
            (data) => {
                this.currUser = JSON.parse(data.text())
            },
            // error
            (error) => {
                window.location.assign('/Login');
            }
            );

    }
}