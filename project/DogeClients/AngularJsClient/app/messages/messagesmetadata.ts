export class MessageMetadata {
        messageId: number;
        fromUser: string;
        toUser: string;
        message: string;
        timeOfSending: Date;
}