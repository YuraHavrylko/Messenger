import {Component} from '@angular/core';
import {HTTP_PROVIDERS, Headers, Response, Http, Request, RequestOptions} from '@angular/http';
import {UserRegistrationComponent} from './auth/signup.component';
import {UserLoginComponent} from './auth/signin.component';
import {FriendsComponent} from './friend/friends.component';
import {MessagesComponent} from './messages/messages.component';
import {BotComponent} from './bot/bot.component';
import {RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS } from '@angular/router-deprecated';

@RouteConfig([
  {
    path: '/Registration',
    name: 'Registration',
    component: UserRegistrationComponent
  },
  {
    path: '/Login',
    name: 'Login',
    component: UserLoginComponent
  },
  {
    path: '/Friends',
    name: 'Friends',
    component: FriendsComponent
  },
  {
    path: '/Messages',
    name: 'Messages',
    component: MessagesComponent
  },
  {
    path: '/Bot',
    name: 'Bot',
    component: BotComponent
  }
])


@Component({
  selector: 'my-app',
  templateUrl: './app/app.component.html',
  directives: [ROUTER_DIRECTIVES],
  providers: [HTTP_PROVIDERS, ROUTER_PROVIDERS]
})

export class AppComponent {

  httpService: Http;

  constructor($http: Http) {
    this.httpService = $http;
  }

  logout(): void {
        sessionStorage.setItem('auth-token-access-token', '');
        sessionStorage.setItem('auth-token-type', '');
        sessionStorage.setItem('auth-token-expires-in', '');
        sessionStorage.setItem('auth-token-refresh-token', '');
        window.location.assign('/Login');
  }
}
