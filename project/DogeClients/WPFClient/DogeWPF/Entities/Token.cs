﻿namespace DogeWPF.Entities
{
    public class Token
    {
        public string access_token { set; get; }
        public string token_type { set; get; }
        public string expires_in { set; get; }
        public string refresh_token { set; get; }

    }
}