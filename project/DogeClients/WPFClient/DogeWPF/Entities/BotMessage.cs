﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DogeWPF.Entities
{
   public class BotMessage
    {
        public string UserMessage { get; set; }
        public string BotAnswer { get; set; }
    }
}
