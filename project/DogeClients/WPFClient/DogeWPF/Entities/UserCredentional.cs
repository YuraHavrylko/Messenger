﻿using System.ComponentModel;
using System.Text.RegularExpressions;

namespace DogeWPF.Entities
{
    public class UserCredentional : INotifyPropertyChanged, IDataErrorInfo
    {
        private string _userName;

        public string UserName
        {
            get { return _userName; }
            set
            {
                _userName = value;
                OnPropertyChanged("UserName");
            }
        }

        private string _password;

        public string Password
        {
            get { return _password; }
            set
            {
                _password = value;
                OnPropertyChanged("Password");
            }
        }

        private string _rePassword;

        public string RePassword
        {
            get { return _rePassword; }
            set
            {
                _rePassword = value;
                OnPropertyChanged("RePassword");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        public string Error => null;


        public string this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    case "UserName":
                        {
                            return ValidateUserName(columnName);
                        }
                    case "Password":
                        {
                            return ValidatePassword(columnName);
                        }
                    case "RePassword":
                        {
                            return ValidateRePassword(columnName);
                        }
                }
                return null;
            }
        }

        private string ValidateUserName(string columnName)
        {
            if (UserName == null) return null;

            return Regex.IsMatch(UserName, @"^[a-z0-9_-]{3,16}$") ? null : "UserName not valid";
        }

        private string ValidatePassword(string columnName)
        {
            if (Password == null) return null;
            return Regex.IsMatch(Password, @"^[a-z0-9_-]{6,18}$") ? null : "Password not valid";
        }
        private string ValidateRePassword(string columnName)
        {
            return RePassword == Password ? null : "Passwords do not match";
        }
    }
}