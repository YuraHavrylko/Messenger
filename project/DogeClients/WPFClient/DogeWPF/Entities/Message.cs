﻿using System;

namespace DogeWPF.Entities
{
    public class Message
    {
        public int messageId { get; set; }
        public string fromUser { get; set; }
        public string toUser { get; set; }
        public string message { get; set; }
        public DateTime timeOfSending { get; set; }
    }
}
