﻿namespace DogeWPF.Entities
{
    public class UserMetadata
    {
        public int userId { set; get; }
        public string userName { set; get; }
    }
}