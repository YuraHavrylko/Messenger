﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DogeWPF.Entities;
using DogeWPF.Services;

namespace DogeWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private async void LoginBTN_Click(object sender, RoutedEventArgs e)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:49768");
                client.DefaultRequestHeaders.Accept.Clear();             
                try
                {
                    var tempUser = new UserCredentional
                    {
                        UserName = LoginUsernameTextBox.Text,
                        Password = LoginPasswordTextBox.Password
                    };
                    Dictionary<string, string> postData = new Dictionary<string, string>();
                    postData.Add("grant_type", "password");
                    postData.Add("username", tempUser.UserName);
                    postData.Add("password", tempUser.Password);

                    var content = new FormUrlEncodedContent(postData);
                    content.Headers.Clear();
                    content.Headers.Add("Content-Type", "application/x-www-form-urlencoded");

                    var response = await client.PostAsync("/api/Auth/Token", content);
                    response.EnsureSuccessStatusCode();

                    var token = response.Content.ReadAsAsync<Token>().Result;
                    TokenService.SetToken(token);

                    var loginedWindow = new LoginedWindow();
                    loginedWindow.Show();
                    Close();

                }
                catch (HttpRequestException ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void RegisterButton_OnClickBTN_Click(object sender, RoutedEventArgs e)
        {
            var regWin = new RegisterWindow();
            regWin.Show();
            Close();
        }
    }
}