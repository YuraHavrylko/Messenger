﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DogeWPF.Entities;


namespace DogeWPF
{
    /// <summary>
    /// Interaction logic for RegisterWindow.xaml
    /// </summary>
    public partial class RegisterWindow : Window
    {

        public RegisterWindow()
        {
            InitializeComponent();
        }



        private async void registerButton_click(object sender, RoutedEventArgs e)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:49768");

                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                if (RegisterPasswordTextBox.Password != RegisterConfirmPasswordTextBox.Password)
                {
                    MessageBox.Show("Password and confirm password doesn't match");
                    return;
                }

                try
                {
                    var tempUser = new UserCredentional
                    {
                        UserName = RegisterUserNameTextBox.Text,
                        Password = RegisterPasswordTextBox.Password,
                        RePassword = RegisterConfirmPasswordTextBox.Password
                    };

                    var response = await client.PostAsJsonAsync("/api/Account/SignUp/", tempUser);
                    response.EnsureSuccessStatusCode();
                    MessageBox.Show("User Registered Successfully", "Result", MessageBoxButton.OK);

                    var mainWindow = new MainWindow();
                    mainWindow.Show();
                    Close();
                }
                catch (HttpRequestException ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void CancelButton_OnClick(object sender, RoutedEventArgs e)
        {
            var mainWindow = new MainWindow();
            mainWindow.Show();
            Close();
        }
    }
}
