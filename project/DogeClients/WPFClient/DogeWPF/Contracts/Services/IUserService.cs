using DogeWPF.Entities;

namespace DogeWPF.Contracts.Services
{
    public interface IUserService
    {
        UserMetadata GetCurrentUser();

        UserMetadata GetUser(int userId);

        UserMetadata GetUserByUserName(string username);

        UserMetadata[] SearchUser(string userName);
    }
}