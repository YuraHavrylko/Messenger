using DogeWPF.Entities;

namespace DogeWPF.Contracts.Services
{
    public interface IMessageService
    {
        void SendMessage(Message message);

        string SendBotMessage(string message);

        Message[] GetDialog(string fromFriend, string toFriend);

        Message[] GetCurrentDialogs();
    }
}