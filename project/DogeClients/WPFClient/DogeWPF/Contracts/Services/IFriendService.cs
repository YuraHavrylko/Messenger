﻿using DogeWPF.Entities;

namespace DogeWPF.Contracts.Services
{
    public interface IFriendService
    {
        UserMetadata[] GetCurrentUserFriends();

        UserMetadata[] GetCommonFriends(UserMetadata friend);

        int GetCountPath(UserMetadata friend);

        UserMetadata[] GetCountAndFriendsPath(UserMetadata friend);

        UserMetadata[] GetUserFriends(UserMetadata friend);
    }
}
