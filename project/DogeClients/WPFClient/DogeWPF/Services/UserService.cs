﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Windows.Forms;
using DogeWPF.Contracts.Services;
using DogeWPF.Entities;

namespace DogeWPF.Services
{
    public class UserService : IUserService
    {
        public UserMetadata GetCurrentUser()
        {
            UserMetadata user = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:49768");

                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var token = TokenService.GetToken();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(token.token_type,
                    token.access_token);
                try
                {
                    var response = client.GetAsync("/api/User/GetCurrentUser").Result;
                    response.EnsureSuccessStatusCode();
                    user = response.Content.ReadAsAsync<UserMetadata>().Result;
                    return user;
                }
                catch (HttpRequestException ex)
                {
                    MessageBox.Show(ex.Message);
                    return user;
                }
            }
        }

        public UserMetadata GetUser(int id)
        {
            UserMetadata tempUser = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:49768");

                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var token = TokenService.GetToken();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(token.token_type,
                    token.access_token);
                try
                {
                    var response = client.GetAsync("/api/User/GetUser/" + id).Result;
                    response.EnsureSuccessStatusCode();
                    tempUser = response.Content.ReadAsAsync<UserMetadata>().Result;
                    return tempUser;
                }
                catch (HttpRequestException ex)
                {
                    MessageBox.Show(ex.Message);
                    return tempUser;
                }

            }
        }

        public UserMetadata GetUserByUserName(string userName)
        {
            UserMetadata tempUser = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:49768");

                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var token = TokenService.GetToken();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(token.token_type,
                    token.access_token);
                try
                {
                    var response = client.GetAsync("/api/User/GetUserByUserName/" + userName).Result;
                    response.EnsureSuccessStatusCode();
                    tempUser = response.Content.ReadAsAsync<UserMetadata>().Result;
                    return tempUser;
                }
                catch (HttpRequestException ex)
                {
                    MessageBox.Show(ex.Message);
                    return tempUser;
                }
            }
        }

        public UserMetadata[] SearchUser(string userName)
        {
            UserMetadata[] tempUsers = { };
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:49768");

                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var token = TokenService.GetToken();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(token.token_type,
                    token.access_token);
                try
                {
                    var response = client.GetAsync("/api/User/GetSearchedUsers/" + userName).Result;
                    response.EnsureSuccessStatusCode();
                    tempUsers = response.Content.ReadAsAsync<UserMetadata[]>().Result;
                    return tempUsers;
                }
                catch (HttpRequestException ex)
                {
                    MessageBox.Show(ex.Message);
                    return tempUsers;
                }
            }
        }
    }
}
