﻿using System;
using System.Windows.Data;
using DogeWPF.Entities;

namespace DogeWPF.Services
{
    public class MessageConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            Message msg = new Message();
            msg.fromUser = values[0].ToString();
            msg.toUser = values[1].ToString();
            return msg;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
