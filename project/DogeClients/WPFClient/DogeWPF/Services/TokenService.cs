﻿using System.IO;
using DogeWPF.Entities;

namespace DogeWPF.Services
{
    public class TokenService
    {
        static Serializer _serializer = new Serializer();
        static string _path = @"..\..\TokenBase.xml";

        public static void SetToken(Token token)
        {
            var serialize = _serializer.Serialize(token);
            File.WriteAllText(_path, serialize);
        }

        public static Token GetToken()
        {
            var token = _serializer.Deserialize<Token>(_path);
            return token;
        }
    }
}