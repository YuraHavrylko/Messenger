﻿using System;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Text;

namespace DogeWPF.Services
{
    public class Serializer 
    {
        public string Serialize<T>(T itemForSerialize) where T : class
        {
            MemoryStream stream = new MemoryStream();
            DataContractJsonSerializer ds = new DataContractJsonSerializer(typeof(T));
            ds.WriteObject(stream, itemForSerialize);
            string jsonString = Encoding.UTF8.GetString(stream.ToArray());
            stream.Close();
            return jsonString;
        }

        public T Deserialize<T>(string path) where T : class
        {
            if (string.IsNullOrEmpty(path) || !File.Exists(path))
            {
                throw new ArgumentException(string.Format("File '{0}' doesn't exist.", path), "path");
            }
            using (FileStream fs = new FileStream(path, FileMode.OpenOrCreate))
            {
                var jsonFormatter = new DataContractJsonSerializer(typeof(T));
                T tObject = (T)jsonFormatter.ReadObject(fs);
                return tObject;
            }
        }
    }
}