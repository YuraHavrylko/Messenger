﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Windows.Forms;
using DogeWPF.Contracts.Services;
using DogeWPF.Entities;

namespace DogeWPF.Services
{
    public class FriendService : IFriendService
    {
        public UserMetadata[] GetCurrentUserFriends()
        {
            UserMetadata[] friends = { };
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:49768");

                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var token = TokenService.GetToken();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(token.token_type, token.access_token);
                try
                {
                    var response = client.GetAsync("/api/FriendShip/GetCurrentUserFriends/").Result;
                    response.EnsureSuccessStatusCode();
                    friends = response.Content.ReadAsAsync<UserMetadata[]>().Result;
                    return friends;
                }
                catch (HttpRequestException ex)
                {
                    MessageBox.Show(ex.Message);
                    return friends;

                }
            }
        }

        public UserMetadata[] GetCommonFriends(UserMetadata friend)
        {
            UserMetadata[] friends = { };

            var friendParameter = friend.userId;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:49768");

                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var token = TokenService.GetToken();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(token.token_type, token.access_token);
                try
                {
                    var response = client.GetAsync("/api/FriendShip/GetCommonFriends/" + friendParameter).Result;
                    response.EnsureSuccessStatusCode();
                    friends = response.Content.ReadAsAsync<UserMetadata[]>().Result;
                    return friends;
                }
                catch (HttpRequestException ex)
                {
                    MessageBox.Show(ex.Message);
                    return friends;

                }
            }
        }

        public int GetCountPath(UserMetadata friend)
        {
            var count = 0;

            var friendParameter = friend.userId;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:49768");

                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var token = TokenService.GetToken();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(token.token_type,
                    token.access_token);
                try
                {
                    var response = client.GetAsync("/api/FriendShip/GetCountAndFriendsPath/" + friendParameter).Result;
                    response.EnsureSuccessStatusCode();
                    count = response.Content.ReadAsAsync<int>().Result;
                    return count;
                }
                catch (HttpRequestException ex)
                {
                    MessageBox.Show(ex.Message);
                    return count;

                }
            }
        }

        public UserMetadata[] GetCountAndFriendsPath(UserMetadata friend)
        {
            UserMetadata[] friends = { };

            var friendParameter = friend.userId;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:49768");

                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var token = TokenService.GetToken();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(token.token_type, token.access_token);
                try
                {
                    var response = client.GetAsync("/api/FriendShip/GetCountAndFriendsPath/" + friendParameter).Result;
                    response.EnsureSuccessStatusCode();
                    friends = response.Content.ReadAsAsync<UserMetadata[]>().Result;
                    return friends;
                }
                catch (HttpRequestException ex)
                {
                    MessageBox.Show(ex.Message);
                    return friends;

                }
            }
        }

        public UserMetadata[] GetUserFriends(UserMetadata friend)
        {
            UserMetadata[] friends = { };

            var friendParameter = friend.userId;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:49768");

                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var token = TokenService.GetToken();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(token.token_type, token.access_token);
                try
                {
                    var response = client.GetAsync("/api/FriendShip/GetUserFriends/" + friendParameter).Result;
                    response.EnsureSuccessStatusCode();
                    friends = response.Content.ReadAsAsync<UserMetadata[]>().Result;
                    return friends;
                }
                catch (HttpRequestException ex)
                {
                    MessageBox.Show(ex.Message);
                    return friends;
                }
            }
        }
    }
}
