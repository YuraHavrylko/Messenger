﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Windows.Forms;
using DogeWPF.Contracts.Services;
using Message = DogeWPF.Entities.Message;

namespace DogeWPF.Services
{
    public class MessageService : IMessageService
    {
        public void SendMessage(Message message)
        {
            using (var client = new HttpClient())
            {

                client.BaseAddress = new Uri("http://localhost:49768");

                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var token = TokenService.GetToken();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(token.token_type, token.access_token);
                try
                {
                    var response = client.PostAsJsonAsync("/api/Message/SendMessage/", message).Result;
                    response.EnsureSuccessStatusCode();

                }
                catch (HttpRequestException ex)
                {
                    MessageBox.Show(ex.Message);

                }
            }
        }

        public string SendBotMessage(string message)
        {
            using (var client = new HttpClient())
            {
                var tempMessage = String.Empty;
                client.BaseAddress = new Uri("http://localhost:49768");

                client.DefaultRequestHeaders.Accept.Clear();
                //client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var token = TokenService.GetToken();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(token.token_type,
                    token.access_token);
                try
                {
                    var response = client.PostAsync("/api/Message/SendBotMessage/", new StringContent(message)).Result;
                    response.EnsureSuccessStatusCode();
                    tempMessage = response.Content.ReadAsAsync<string>().Result;
                    return tempMessage;


                }
                catch (HttpRequestException ex)
                {
                    MessageBox.Show(ex.Message);
                    return tempMessage;
                }
            }
        }

        public Message[] GetDialog(string fromUser, string toUser)
        {
            Message[] messages = { };

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:49768");

                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var token = TokenService.GetToken();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(token.token_type, token.access_token);
                try
                {
                  var response = client.GetAsync("/api/Message/GetDialog/" + fromUser + "/" + toUser).Result;
                    response.EnsureSuccessStatusCode();
                    messages = response.Content.ReadAsAsync<Message[]>().Result;
                    return messages;
                }
                catch (HttpRequestException ex)
                {
                    MessageBox.Show(ex.Message);
                    return messages;
                }
            }
        }

        public Message[] GetCurrentDialogs()
        {
            Message[] messages = { };
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:49768");

                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var token = TokenService.GetToken();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(token.token_type, token.access_token);
                try
                {
                    var response = client.GetAsync("/api/Message/GetCurrentDialogs").Result;
                    response.EnsureSuccessStatusCode();
                    messages = response.Content.ReadAsAsync<Message[]>().Result;
                    return messages;
                }
                catch (HttpRequestException ex)
                {
                    MessageBox.Show(ex.Message);
                    return messages;
                }
            }
        }
    }
}
