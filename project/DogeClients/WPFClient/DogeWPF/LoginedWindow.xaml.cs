﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using DogeWPF.Entities;
using DogeWPF.Services;
using DogeWPF.Contracts.Services;

namespace DogeWPF
{
    /// <summary>
    /// Interaction logic for LoginedWindow.xaml
    /// </summary>
    public partial class LoginedWindow : Window
    {
        public UserMetadata CurentUserName;
        private string currentFriend;
        private List<BotMessage> answersBotMessages = new List<BotMessage>();

        private IUserService _userService = new UserService();
        private IFriendService _friendService = new FriendService();
        private IMessageService _messageService = new MessageService();


        public LoginedWindow()
        {
            CurentUserName = _userService.GetCurrentUser();
            InitializeComponent();

            FriendsPanelLoad();
        }

        private void FriendsPanelLoad()
        {
            SetStackPanelsVisibility(FriendsStackPanel);

            var friends = _friendService.GetCurrentUserFriends();
            FriendsListBox.ItemsSource = friends;
        }

        private void MessagesButton_OnClick(object sender, RoutedEventArgs e)
        {
            SetStackPanelsVisibility(MessagesStackPanel);
            RefreshDialogs();
        }

        private void FriendsButton_OnClick(object sender, RoutedEventArgs e)
        {
            FriendsPanelLoad();
        }

        private void TalkWithBotButton_OnClick(object sender, RoutedEventArgs e)
        {
            SetStackPanelsVisibility(TalkWithBotStackPanel);

        }

        private void LogOutButton_OnClickButton_OnClick(object sender, RoutedEventArgs e)
        {
            var mainWindow = new MainWindow();
            mainWindow.Show();
            Close();
        }

        private void SetStackPanelsVisibility(StackPanel stackPanel)
        {
            FriendsStackPanel.Visibility = Visibility.Collapsed;
            MessagesStackPanel.Visibility = Visibility.Collapsed;
            TalkWithBotStackPanel.Visibility = Visibility.Collapsed;

            stackPanel.Visibility = Visibility.Visible;
        }

        private void CommonFriendsButton_OnClick(object sender, RoutedEventArgs e)
        {
            var selectedUser = _userService.GetUserByUserName(Convert.ToString(((Button)sender).Tag));

            var friends = _friendService.GetCommonFriends(selectedUser);

            FriendsToStackPanelTest.Visibility = Visibility.Visible;
            FriendsListBox.ItemsSource = friends;
        }

        private void UserFriendsButton_OnClick(object sender, RoutedEventArgs e)
        {
            var selectedUser = _userService.GetUserByUserName(Convert.ToString(((Button)sender).Tag));

            var friends = _friendService.GetUserFriends(selectedUser);

            FriendsToStackPanelTest.Visibility = Visibility.Visible;
            FriendsListBox.ItemsSource = friends;
        }

        private void OpenDialogButtonClick(object sender, RoutedEventArgs e)
        {
            RefreshDialogs();

            Message msg = ((Button)sender).Tag as Message;

            currentFriend = msg.fromUser == CurentUserName.userName ? msg.toUser : msg.fromUser;

            var currentDialog = _messageService.GetDialog(msg.fromUser, msg.toUser);
            CurrenrDialogMessagesListBox.ItemsSource = currentDialog;
            CurrenrDialogMessagesListBox.Items.MoveCurrentToLast();
            CurrenrDialogMessagesListBox.ScrollIntoView(CurrenrDialogMessagesListBox.Items.CurrentItem);
        }

        private void SendMessageButton_OnClick(object sender, RoutedEventArgs e)
        {
            var tempMessage = new Message();
            tempMessage.message = WriteMessageTextbox.Text;
            tempMessage.fromUser = CurentUserName.userName;
            tempMessage.toUser = currentFriend;

            _messageService.SendMessage(tempMessage);
            WriteMessageTextbox.Text = string.Empty;

            var currentDialog = _messageService.GetDialog(CurentUserName.userName, currentFriend);
            CurrenrDialogMessagesListBox.ItemsSource = currentDialog;
            CurrenrDialogMessagesListBox.Items.MoveCurrentToLast();
            CurrenrDialogMessagesListBox.ScrollIntoView(CurrenrDialogMessagesListBox.Items.CurrentItem);
            RefreshDialogs();
        }

        private void SndMsgButton_OnClick(object sender, RoutedEventArgs e)
        {
            var tempMessage = new Message();
            tempMessage.message = writeMsgTextbox.Text;
            tempMessage.fromUser = CurentUserName.userName;
            tempMessage.toUser = currentFriend;

            _messageService.SendMessage(tempMessage);
            writeMsgTextbox.Text = string.Empty;

            var currentDialog = _messageService.GetDialog(CurentUserName.userName, currentFriend);
            FriendMessageListBox.ItemsSource = currentDialog;
            FriendMessageListBox.Items.MoveCurrentToLast();
            FriendMessageListBox.ScrollIntoView(FriendMessageListBox.Items.CurrentItem);
        }

        private void RefreshDialogs()
        {
            var dialogs = _messageService.GetCurrentDialogs().Reverse();
            DialogsListBox.ItemsSource = dialogs;
        }

        private void SearchShortestPathButton_OnClick(object sender, RoutedEventArgs e)
        {
            var selectedUser = _userService.GetUserByUserName(Convert.ToString(((Button)sender).Tag));
            var friends = _friendService.GetCountAndFriendsPath(selectedUser);

            int pathLength = friends.Length - 1;

            if (pathLength >= 0)
            {
                PathLabel.Content = "Length of path to user is: " + pathLength;
            }
            else
            {
                PathLabel.Content = "It is you";
            }


            FriendMessageStackPanel.Visibility = Visibility.Collapsed;
            PathStackPanel.Visibility = Visibility.Visible;
            PathListBox.ItemsSource = friends;
        }

        private void SendMessageToBotButton_OnClick(object sender, RoutedEventArgs e)
        {
            var tempMessage = WriteMessagetoBotTextbox.Text;
            var answer = _messageService.SendBotMessage(tempMessage);
            answersBotMessages.Add(new BotMessage { UserMessage = tempMessage, BotAnswer = answer });
            AnswerFromBotListBox.ItemsSource = answersBotMessages;
            if (answersBotMessages.Count > 1)
            {
                AnswerFromBotListBox.Items.Refresh();
            }

            WriteMessagetoBotTextbox.Text = string.Empty;

            AnswerFromBotListBox.Items.MoveCurrentToLast();
            AnswerFromBotListBox.ScrollIntoView(AnswerFromBotListBox.Items.CurrentItem);
        }

        private void OpenDialogInFriendsStackPanelButtonClick(object sender, RoutedEventArgs e)
        {
            currentFriend = Convert.ToString(((Button)sender).Tag);
            var currentDialog = _messageService.GetDialog(CurentUserName.userName, currentFriend);

            MessageLabel.Content = "Dialog with " + currentFriend;

            PathStackPanel.Visibility = Visibility.Collapsed;
            FriendMessageStackPanel.Visibility = Visibility.Visible;

            FriendMessageListBox.ItemsSource = currentDialog;
            FriendMessageListBox.Items.MoveCurrentToLast();
            FriendMessageListBox.ScrollIntoView(FriendMessageListBox.Items.CurrentItem);
        }

        private void SearchUsersTextBox_TextChanged(object sender, RoutedEventArgs e)
        {
            string query = SearchUsersTextBox.Text;

            if (query != string.Empty)
            {
                var userList = _userService.SearchUser(query);
                FriendsListBox.ItemsSource = userList;
            }
            else
            {
                FriendsListBox.ItemsSource = _friendService.GetCurrentUserFriends();
            }
        }
    }
}
